import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from 'devextreme/data/custom_store';

export const apiDashSocmed_AgentOnline = createAsyncThunk(
    "dashboard/apiDashSocmed_AgentOnline",
    async () => {
        const res = await axios.get('/dashboard/socmed/agent_online');
        return res.data;
    }
)

export const apiDashSocmed_TotalChannel = createAsyncThunk(
    "dashboard/apiDashSocmed_TotalChannel",
    async () => {
        const res = await axios.get('/dashboard/socmed/total_channel');
        return res.data;
    }
)

export const apiDashSocmed_ChannelInteraction = createAsyncThunk(
    "dashboard/apiDashSocmed_ChannelInteraction",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/dashboard/socmed/channel_interaction`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)


export const apiDashTotalTicket = createAsyncThunk(
    "dashboard/apiDashTotalTicket",
    async (data) => {
        const res = await axios.post('/dashboard/ticket/total_ticket', data);
        return res.data;
    }
)

export const apiDashDataTicket = createAsyncThunk(
    "dashboard/apiDashDataTicket",
    async (data) => {
        const res = await axios.post('/dashboard/ticket/data_ticket_sla', data);
        return res.data;
    }
)

export const apiDashChartChannel = createAsyncThunk(
    "dashboard/apiDashChartChannel",
    async (data) => {
        const res = await axios.post('/dashboard/ticket/chart_by_channel', data);
        return res.data;
    }
)

export const apiDashChartTotal = createAsyncThunk(
    "dashboard/apiDashChartTotal",
    async (data) => {
        const res = await axios.post('/dashboard/ticket/chart_by_total', data);
        return res.data;
    }
)

export const apiDashDataAnalitycSentiment = createAsyncThunk(
    "dashboard/apiDashDataAnalitycSentiment",
    async (data) => {
        const res = await axios.post('/dashboard/analityc/data_analysis_sentiment', data);
        return res.data;
    }
)

export const apiDashTotalBySentiment = createAsyncThunk(
    "dashboard/apiDashTotalBySentiment",
    async (data) => {
        const res = await axios.post('/dashboard/analityc/total_by_sentiment', data);
        return res.data;
    }
)

export const apiDashSentimentByChannel = createAsyncThunk(
    "dashboard/apiDashSentimentByChannel",
    async (data) => {
        const res = await axios.post('/dashboard/analityc/sentiment_by_channel', data);
        return res.data;
    }
)

export const apiDashSentimentByWeek = createAsyncThunk(
    "dashboard/apiDashSentimentByWeek",
    async (data) => {
        const res = await axios.post('/dashboard/analityc/sentiment_by_week', data);
        return res.data;
    }
)

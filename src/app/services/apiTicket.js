import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiTicketList = createAsyncThunk(
    "ticket/apiTicketList",
    async () => {
        const res = await axios.get('/ticket');
        return res.data;
    }
)

export const apiTicketStore = createAsyncThunk(
    "ticket/apiTicketStore",
    async (data) => {
        const json = JSON.stringify(data);
        const res = await axios.post('/ticket/store', json);
        return res.data;
    }
)

export const apiTicketUpdate = createAsyncThunk(
    "ticket/apiTicketUpdate",
    async (data) => {
        const res = await axios.put('/ticket/update', data);
        return res.data;
    }
)

export const apiTicketShow = createAsyncThunk(
    "ticket/apiTicketShow",
    async ({ ticket_number }) => {
        const res = await axios.get(`/ticket/show/${ticket_number}`);
        return res.data;
    }
)

export const apiLastResponseAgent = createAsyncThunk(
    "ticket/apiLastResponseAgent",
    async (params) => {
        const res = await axios.post(`/ticket/last_response`, JSON.stringify(params));
        return res.data;
    }
)

export const apiHistoryTransaction = createAsyncThunk(
    "ticket/apiHistoryTransaction",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/ticket/transaction`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
)

export const apiPublish = createAsyncThunk(
    "ticket/apiPublish",
    async ({ customer_id }) => {
        const json = JSON.stringify({ customer_id });
        const res = await axios.post(`/ticket/publish`, json);
        return res.data;
    }
)

export const apiDataPublish = createAsyncThunk(
    "ticket/apiDataPublish",
    async ({ customer_id }) => {
        const res = await axios.get(`/ticket/publish/${customer_id}`);
        return res.data;
    }
)

export const apiInteraction = createAsyncThunk(
    "ticket/apiInteraction",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/ticket/interaction`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
)

export const apiEscalation = createAsyncThunk(
    "ticket/apiEscalation",
    async (data) => {
        const res = await axios.put(`/ticket/escalation`, data);
        return res.data;
    }
)

export const apiHistoryTicket = createAsyncThunk(
    "ticket/apiHistoryTicket",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/ticket/history`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
)

export const apiHistoryTicketExport = createAsyncThunk(
    "ticket/apiHistoryTicketExport",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post(`/ticket/history_export`, json);
        return res.data;
    }
);

export const apiReferenceTicket = createAsyncThunk(
    "ticket/apiReferenceTicket",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/ticket/reference`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
);

export const apiMassDistruption = createAsyncThunk(
    "ticket/apiMassDistruption",
    async () => {
        const res = await axios.post('/ticket/mass_distruption');
        return res.data;
    }
);

export const apiInteractionUser = createAsyncThunk(
    "ticket/apiInteractionUser",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post('/ticket/interaction_user', json);
        return res.data;
    }
);

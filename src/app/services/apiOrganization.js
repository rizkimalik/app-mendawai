import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiOrganizationList = createAsyncThunk(
    "organization/apiOrganizationList",
    async () => {
        const res = await axios.get('/organization');
        return res.data;
    }
)

export const apiOrganizationShow = createAsyncThunk(
    "organization/apiOrganizationShow",
    async ({ id }) => {
        const res = await axios.get(`/organization/show/${id}`);
        return res.data;
    }
)

export const apiOrganizationStore = createAsyncThunk(
    "organization/apiOrganizationStore",
    async (organization) => {
        const json = JSON.stringify(organization);
        const res = await axios.post('/organization/store', json);
        return res.data;
    }
)

export const apiOrganizationUpdate = createAsyncThunk(
    "organization/apiOrganizationUpdate",
    async (organization) => {
        const res = await axios.put('/organization/update', organization);
        return res.data;
    }
)

export const apiOrganizationDelete = createAsyncThunk(
    "organization/apiOrganizationDelete",
    async ({ id }) => {
        const res = await axios.delete(`/organization/delete/${id}`);
        return res.data;
    }
)

import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiStatusList = createAsyncThunk(
    "status/apiStatusList",
    async () => {
        const res = await axios.get('/status');
        return res.data;
    }
)

export const apiStatusShow = createAsyncThunk(
    "status/apiStatusShow",
    async ({ id }) => {
        const res = await axios.get(`/status/show/${id}`);
        return res.data;
    }
)

export const apiStatusStore = createAsyncThunk(
    "status/apiStatusStore",
    async (status) => {
        const json = JSON.stringify(status);
        const res = await axios.post('/status/store', json);
        return res.data;
    }
)

export const apiStatusUpdate = createAsyncThunk(
    "status/apiStatusUpdate",
    async (status) => {
        const res = await axios.put('/status/update', status);
        return res.data;
    }
)

export const apiStatusDelete = createAsyncThunk(
    "status/apiStatusDelete",
    async ({ id }) => {
        const res = await axios.delete(`/status/delete/${id}`);
        return res.data;
    }
)

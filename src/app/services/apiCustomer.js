import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from 'devextreme/data/custom_store';

export const apiCustomerList = createAsyncThunk(
    "customer/apiCustomerList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/customer`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiCustomerRegistered = createAsyncThunk(
    "customer/apiCustomerRegistered",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/customer/data_registered`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiCustomerShow = createAsyncThunk(
    "customer/apiCustomerShow",
    async ({ customer_id }) => {
        const res = await axios.get(`/customer/show/${customer_id}`);
        return res.data;
    }
)

export const apiCustomerStore = createAsyncThunk(
    "customer/apiCustomerStore",
    async (customer) => {
        const json = JSON.stringify(customer);
        const res = await axios.post('/customer/store', json);
        return res.data;
    }
)

export const apiCustomerUpdate = createAsyncThunk(
    "customer/apiCustomerUpdate",
    async (customer) => {
        const res = await axios.put('/customer/update', customer);
        return res.data;
    }
)

export const apiCustomerDelete = createAsyncThunk(
    "customer/apiCustomerDelete",
    async ({ customer_id }) => {
        const res = await axios.delete(`/customer/delete/${customer_id}`);
        return res.data;
    }
)

export const apiCustomerChannel = createAsyncThunk(
    "customer/apiCustomerChannel",
    async () => {
        const store = new CustomStore({
            key: 'id',
            load: async () => {
                return await axios.post(`/customer/channel`)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiCustomerJourney = createAsyncThunk(
    "customer/apiCustomerJourney",
    async ({ customer_id }) => {
        const res = await axios.get(`/customer/journey/${customer_id}`);
        return res.data;
    }
)

// test dxgrid
export const apiCustomerDxGrid = createAsyncThunk(
    "customer/apiCustomerDxGrid",
    async () => {
        function isNotEmpty(value) {
            return value !== undefined && value !== null && value !== '';
        }

        const store = new CustomStore({
            key: 'id',
            load: async (loadOptions) => {
                let params = '?';
                [
                    'skip',
                    'take',
                    'requireTotalCount',
                    'requireGroupCount',
                    'sort',
                    'filter',
                    'totalSummary',
                    'group',
                    'groupSummary',
                ].forEach((i) => {
                    if (i in loadOptions && isNotEmpty(loadOptions[i])) { params += `${i}=${JSON.stringify(loadOptions[i])}&`; }
                });
                params = params.slice(0, -1);

                return await axios.get(`/customer/data_grid${params}`)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading Error'); });
            },
        });

        return store;
    }
)

export const apiCustomerDataExport = createAsyncThunk(
    "customer/apiCustomerDataExport",
    async () => {
        const res = await axios.get('/customer/data_export');
        return res.data;
    }
)

export const apiCustomer_DataChannel = createAsyncThunk(
    "customer/apiCustomer_DataChannel",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/customer/data_channel`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                let params = {
                    ...param,
                    ...values
                }
                return await axios.post(`/customer/insert_channel`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            }, 
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values
                }
                return await axios.post(`/customer/update_channel`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (key) => {
                return await axios.post(`/customer/delete_channel`, JSON.stringify({ id: key }))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiCustomerSyncronize = createAsyncThunk(
    "customer/apiCustomerSyncronize",
    async (params) => {
        const res = await axios.post('/customer/syncronize', JSON.stringify(params));
        return res.data;
    }
)

export const apiCustomer_DataSync = createAsyncThunk(
    "customer/apiCustomer_DataSync",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/customer/syncronize_data`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)
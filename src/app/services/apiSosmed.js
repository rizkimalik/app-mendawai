import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const getListCustomer = createAsyncThunk(
    "sosialmedia/getListCustomer",
    async ({ agent_handle }) => {
        const res = await axios.post('/omnichannel/list_customers', { agent_handle });
        return res.data;
    }
)

export const getLoadConversation = createAsyncThunk(
    "sosialmedia/getLoadConversation",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post('/omnichannel/conversation_chats', { chat_id, customer_id });
        return res.data;
    }
)

export const getEndChat = createAsyncThunk(
    "sosialmedia/getEndChat",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post('/omnichannel/end_chat', { chat_id, customer_id });
        return res.data;
    }
)

export const getDataMessages = createAsyncThunk(
    "sosialmedia/getDataMessages",
    async (param) => {
        const store = new CustomStore({
            key: 'chat_id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/data_messages`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiInteractionDetail = createAsyncThunk(
    "sosialmedia/apiInteractionDetail",
    async (param) => {
        const res = await axios.post('/omnichannel/interaction_detail', JSON.stringify(param));
        return res.data;
    }
);

export const apiInsertCallHistory = createAsyncThunk(
    "sosialmedia/apiInsertCallHistory",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post('/webhook/call/get_history', json);
        return res.data;
    }
)

export const getCallHistoryData = createAsyncThunk(
    "sosialmedia/getCallHistoryData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/call/data_call`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
)

export const apiInsertCustomerEmail = createAsyncThunk(
    "sosialmedia/apiInsertCustomerEmail",
    async (param) => {
        const res = await axios.post('/omnichannel/email/insert_customer', JSON.stringify(param));
        return res.data;
    }
)

export const getEmailInboxData = createAsyncThunk(
    "sosialmedia/getEmailInboxData",
    async (param) => {
        const store = new CustomStore({
            key: 'IVC_ID',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_inbox`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const getEmailSentData = createAsyncThunk(
    "sosialmedia/getEmailSentData",
    async (param) => {
        const store = new CustomStore({
            key: 'IVC_ID',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_sent`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const getCallTotal = createAsyncThunk(
    "sosialmedia/getCallTotal",
    async () => {
        const res = await axios.post('/omnichannel/call/total_call');
        return res.data;
    }
)

export const getSosmedNotification = createAsyncThunk(
    "sosialmedia/getSosmedNotification",
    async (param) => {
        const res = await axios.post('/omnichannel/sosmed_notifications', JSON.stringify(param));
        return res.data;
    }
)

export const setAccountSpam = createAsyncThunk(
    "sosialmedia/setAccountSpam",
    async (params) => {
        const res = await axios.post('/omnichannel/set_account_spam', JSON.stringify(params));
        return res.data;
    }
)

export const getPabxToken = async () => {
    const res = await axios.post('/omnichannel/call/pabx_get_token');
    return res.data.data;
}

export const getPabxRecording = async (param) => {
    const res = await axios.post('/omnichannel/call/pabx_get_recording', JSON.stringify(param));
    return res.data.data.items[0];
}
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiReportSLA = createAsyncThunk(
    "report/apiReportSLA",
    async (param) => {
        const store = new CustomStore({
            key: 'ticket_number',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/report/sla_grid`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
);

export const apiReportSLAExport = createAsyncThunk(
    "report/apiReportSLAExport",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post(`/report/sla_export`, json);
        return res.data;
    }
);

export const apiReportInteraction = createAsyncThunk(
    "report/apiReportInteraction",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/report/interaction_grid`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
);

export const apiReportInteractionExport = createAsyncThunk(
    "report/apiReportInteractionExport",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post(`/report/interaction_export`, json);
        return res.data;
    }
);

export const apiReportOnProgress = createAsyncThunk(
    "report/apiReportOnProgress",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/report/onprogress_grid`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
);

export const apiReportOnProgressExport = createAsyncThunk(
    "report/apiReportOnProgressExport",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post(`/report/onprogress_export`, json);
        return res.data;
    }
);

export const apiReportByAgent = createAsyncThunk(
    "report/apiReportByAgent",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/report/by_agent_grid`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
);

export const apiReportByAgent_Export = createAsyncThunk(
    "report/apiReportByAgent_Export",
    async (param) => {
        const res = await axios.post(`/report/by_agent_export`, JSON.stringify(param));
        return res.data;
    }
);

export const apiReportAgentHandleChannel = createAsyncThunk(
    "report/apiReportAgentHandleChannel",
    async (param) => {
        const res = await axios.post(`/report/agent_handle_channel`, JSON.stringify(param));
        return res.data;
    }
);

export const apiReportDataChannel = createAsyncThunk(
    "report/apiReportDataChannel",
    async (param) => {
        const res = await axios.post(`/report/data_channel_chart`, JSON.stringify(param));
        return res.data;
    }
);

export const apiReportSpeedAnswerCall = createAsyncThunk(
    "report/apiReportSpeedAnswerCall",
    async (param) => {
        const res = await axios.post(`/report/speed_answer_call`, JSON.stringify(param));
        return res.data;
    }
);

export const apiReportSpeedAnswerMessages = createAsyncThunk(
    "report/apiReportSpeedAnswerMessages",
    async (param) => {
        const res = await axios.post(`/report/speed_answer_messages`, JSON.stringify(param));
        return res.data;
    }
);

export const apiReportSentimentAnalysis = createAsyncThunk(
    "report/apiReportSentimentAnalysis",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/report/sentiment_analysis`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
);

export const apiReportSentimentAnalysisExport = createAsyncThunk(
    "report/apiReportSentimentAnalysisExport",
    async (param) => {
        const res = await axios.post(`/report/sentiment_analysis_export`, JSON.stringify(param));
        return res.data;
    }
);

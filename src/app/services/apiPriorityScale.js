import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiPriorityScaleList = createAsyncThunk(
    "priority_scale/apiPriorityScaleList",
    async () => {
        const res = await axios.get('/priority_scale');
        return res.data;
    }
)

export const apiPriorityScaleShow = createAsyncThunk(
    "priority_scale/apiPriorityScaleShow",
    async ({ id }) => {
        const res = await axios.get(`/priority_scale/show/${id}`);
        return res.data;
    }
)

export const apiPriorityScaleStore = createAsyncThunk(
    "priority_scale/apiPriorityScaleStore",
    async (priority_scale) => {
        const json = JSON.stringify(priority_scale);
        const res = await axios.post('/priority_scale/store', json);
        return res.data;
    }
)

export const apiPriorityScaleUpdate = createAsyncThunk(
    "priority_scale/apiPriorityScaleUpdate",
    async (priority_scale) => {
        const res = await axios.put('/priority_scale/update', priority_scale);
        return res.data;
    }
)

export const apiPriorityScaleDelete = createAsyncThunk(
    "priority_scale/apiPriorityScaleDelete",
    async ({ id }) => {
        const res = await axios.delete(`/priority_scale/delete/${id}`);
        return res.data;
    }
)

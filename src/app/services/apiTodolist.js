import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from 'devextreme/data/custom_store';

export const apiTodolistTotalTicket = createAsyncThunk(
    "todolist/apiTodolistTotalTicket",
    async (data) => {
        const res = await axios.post('/todolist/total_ticket', data);
        return res.data;
    }
)

export const apiTodolistDataTicket = createAsyncThunk(
    "todolist/apiTodolistDataTicket",
    async (param) => {
        // const res = await axios.post('/todolist/data_ticket', data);
        // return res.data;

        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/todolist/data_ticket`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiTodolistNotification = createAsyncThunk(
    "todolist/apiTodolistNotification",
    async (param) => {
        const res = await axios.post('/todolist/data_ticket', param);
        return res.data;
    }
)

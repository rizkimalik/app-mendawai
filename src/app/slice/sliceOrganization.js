import { createSlice } from "@reduxjs/toolkit";
import { apiOrganizationDelete, apiOrganizationList, apiOrganizationShow, apiOrganizationStore, apiOrganizationUpdate } from "app/services/apiOrganization";


const sliceOrganization = createSlice({
    name: "department",
    initialState: {
        response: {},
        organizations: [],
    },
    extraReducers: {
        [apiOrganizationList.fulfilled]: (state, action) => {
            state.organizations = action.payload.data
        },
        [apiOrganizationShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiOrganizationStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiOrganizationUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiOrganizationDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceOrganization;
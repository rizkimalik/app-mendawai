import { createSlice } from "@reduxjs/toolkit";
import { apiTodolistDataTicket, apiTodolistNotification, apiTodolistTotalTicket } from "app/services/apiTodolist";


const sliceTodolist = createSlice({
    name: "todolist",
    initialState: {
        total_ticket: [],
        data_ticket: [],
        data_notification: [],
    },
    extraReducers: {
        [apiTodolistTotalTicket.fulfilled]: (state, action) => {
            state.total_ticket = action.payload.data
        },
        [apiTodolistDataTicket.fulfilled]: (state, action) => {
            state.data_ticket = action.payload
        },
        [apiTodolistNotification.fulfilled]: (state, action) => {
            state.data_notification = action.payload.data
        },
    },
});

export default sliceTodolist;
import { createSlice } from "@reduxjs/toolkit";
import { apiReportSpeedAnswerCall, apiReportAgentHandleChannel, apiReportByAgent, apiReportDataChannel, apiReportInteraction, apiReportOnProgress, apiReportSLA, apiReportSpeedAnswerMessages, apiReportSentimentAnalysis } from "app/services/apiReport";

const sliceReport = createSlice({
    name: "report",
    initialState: {
        report_sla: [],
        report_interaction: [],
        report_onprogress: [],
        report_by_agent: [],
        report_agent_handle_channel: [],
        report_data_channel: [],
        report_speed_answer_call: [],
        report_speed_answer_messages: [],
        report_sentiment_analysis: [],
    },

    extraReducers: {
        [apiReportSLA.fulfilled]: (state, action) => {
            state.report_sla = action.payload
        },
        [apiReportInteraction.fulfilled]: (state, action) => {
            state.report_interaction = action.payload
        },
        [apiReportOnProgress.fulfilled]: (state, action) => {
            state.report_onprogress = action.payload
        },
        [apiReportByAgent.fulfilled]: (state, action) => {
            state.report_by_agent = action.payload
        }, 
        [apiReportAgentHandleChannel.fulfilled]: (state, action) => {
            state.report_agent_handle_channel = action.payload.data
        },
        [apiReportDataChannel.fulfilled]: (state, action) => {
            state.report_data_channel = action.payload.data
        },
        [apiReportSpeedAnswerCall.fulfilled]: (state, action) => {
            state.report_speed_answer_call = action.payload.data
        },
        [apiReportSpeedAnswerMessages.fulfilled]: (state, action) => {
            state.report_speed_answer_messages = action.payload.data
        },
        [apiReportSentimentAnalysis.fulfilled]: (state, action) => {
            state.report_sentiment_analysis = action.payload
        },
    },
});

export default sliceReport;
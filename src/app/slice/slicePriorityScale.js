import { createSlice } from "@reduxjs/toolkit";
import { apiPriorityScaleDelete, apiPriorityScaleList, apiPriorityScaleShow, apiPriorityScaleStore, apiPriorityScaleUpdate } from "app/services/apiPriorityScale";


const slicePriorityScale = createSlice({
    name: "priority_scale",
    initialState: {
        response: {},
        priority_scale: [],
    },
    extraReducers: {
        [apiPriorityScaleList.fulfilled]: (state, action) => {
            state.priority_scale = action.payload.data
        },
        [apiPriorityScaleShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiPriorityScaleStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiPriorityScaleUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiPriorityScaleDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default slicePriorityScale;
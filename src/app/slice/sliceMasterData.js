import { createSlice } from "@reduxjs/toolkit";
import {
    apiMasterChannel,
    apiMasterChannelType,
    apiMasterCustomerType,
    apiMasterPriorityScale,
    apiMasterStatus,
    apiMasterUserLevel,
    apiMasterDepartment,
    apiMasterOrganization,
    apiMasterUserLayer,
} from "app/services/apiMasterData";

const sliceMasterData = createSlice({
    name: "master",
    initialState: {
        channels: [],
        channel_type: [],
        status: [],
        user_level: [],
        user_layer: [],
        priority_scale: [],
        customer_type: [],
        organizations: [],
        departments: [],
    },
    extraReducers: {
        [apiMasterChannel.fulfilled]: (state, action) => {
            state.channels = action.payload.data
        },
        [apiMasterChannelType.fulfilled]: (state, action) => {
            state.channel_type = action.payload.data
        },
        [apiMasterStatus.fulfilled]: (state, action) => {
            state.status = action.payload.data
        },
        [apiMasterUserLevel.fulfilled]: (state, action) => {
            state.user_level = action.payload.data
        },
        [apiMasterUserLayer.fulfilled]: (state, action) => {
            state.user_layer = action.payload.data
        },
        [apiMasterOrganization.fulfilled]: (state, action) => {
            state.organizations = action.payload.data
        },
        [apiMasterDepartment.fulfilled]: (state, action) => {
            state.departments = action.payload.data
        },
        [apiMasterCustomerType.fulfilled]: (state, action) => {
            state.customer_type = action.payload.data
        },
        [apiMasterPriorityScale.fulfilled]: (state, action) => {
            state.priority_scale = action.payload.data
        },
    },
});

export default sliceMasterData;
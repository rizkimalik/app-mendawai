import { createSlice } from "@reduxjs/toolkit";
import { apiDashSocmed_AgentOnline, apiDashSocmed_TotalChannel, apiDashSocmed_ChannelInteraction, apiDashTotalTicket, apiDashDataTicket, apiDashChartChannel, apiDashChartTotal, apiDashDataAnalitycSentiment, apiDashTotalBySentiment, apiDashSentimentByChannel, apiDashSentimentByWeek } from "app/services/apiDashboard";


const sliceDashboard = createSlice({
    name: "dashboard",
    initialState: {
        agent_online: [],
        total_channel: [],
        channel_interaction: [],
        total_ticket: [],
        data_ticket: [],
        chart_by_channel: [],
        chart_by_total: [],
        data_analityc_sentiment: [],
        total_by_sentiment: [],
        sentiment_by_channel: [],
        sentiment_by_week: [],
    },
    extraReducers: {
        [apiDashSocmed_AgentOnline.fulfilled]: (state, action) => {
            state.agent_online = action.payload.data
        },
        [apiDashSocmed_TotalChannel.fulfilled]: (state, action) => {
            state.total_channel = action.payload.data
        },
        [apiDashSocmed_ChannelInteraction.fulfilled]: (state, action) => {
            state.channel_interaction = action.payload
        },
        [apiDashTotalTicket.fulfilled]: (state, action) => {
            state.total_ticket = action.payload.data
        },
        [apiDashDataTicket.fulfilled]: (state, action) => {
            state.data_ticket = action.payload.data
        },
        [apiDashChartChannel.fulfilled]: (state, action) => {
            state.chart_by_channel = action.payload.data
        },
        [apiDashChartTotal.fulfilled]: (state, action) => {
            state.chart_by_total = action.payload.data
        },
        [apiDashDataAnalitycSentiment.fulfilled]: (state, action) => {
            state.data_analityc_sentiment = action.payload.data
        },
        [apiDashTotalBySentiment.fulfilled]: (state, action) => {
            state.total_by_sentiment = action.payload.data
        },
        [apiDashSentimentByChannel.fulfilled]: (state, action) => {
            state.sentiment_by_channel = action.payload.data
        },
        [apiDashSentimentByWeek.fulfilled]: (state, action) => {
            state.sentiment_by_week = action.payload.data
        },
    },
});

export default sliceDashboard;
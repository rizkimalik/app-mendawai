import { createSlice } from "@reduxjs/toolkit";
import { getListCustomer, getLoadConversation, getCallHistoryData, getCallTotal, getSosmedNotification, getEmailInboxData, getEmailSentData, getDataMessages, apiInteractionDetail } from 'app/services/apiSosmed'

const sosmedSlice = createSlice({
    name: "sosialmedia",
    initialState: {
        status: {
            connected: false,
            socket_id: null
        },
        selected_customer: {},
        list_customers: [],
        conversations: [],
        call_history: [],
        total_call: [],
        sosmed_notifications: [],
        email_inbox: [],
        email_sent: [],
        data_messages: [],
        interaction_detail: [],
    },
    reducers: {
        setSocketStatus: (state, action) => {
            state.status = action.payload;
        },
        setSelectedCustomer: (state, action) => {
            state.selected_customer = action.payload;
        },
    },
    extraReducers: {
        [getListCustomer.fulfilled]: (state, action) => {
            state.list_customers = action.payload
        },
        [getLoadConversation.fulfilled]: (state, action) => {
            state.conversations = action.payload
        },
        [getCallHistoryData.fulfilled]: (state, action) => {
            state.call_history = action.payload
        },
        [getCallTotal.fulfilled]: (state, action) => {
            state.total_call = action.payload.data
        },
        [getSosmedNotification.fulfilled]: (state, action) => {
            state.sosmed_notifications = action.payload.data
        },
        [getEmailInboxData.fulfilled]: (state, action) => {
            state.email_inbox = action.payload
        },
        [getEmailSentData.fulfilled]: (state, action) => {
            state.email_sent = action.payload
        },
        [getDataMessages.fulfilled]: (state, action) => {
            state.data_messages = action.payload
        },
        [apiInteractionDetail.fulfilled]: (state, action) => {
            state.interaction_detail = action.payload.data
        },
    },
});

//export actions & reducer
export const {
    setSocketStatus,
    setSelectedCustomer,
} = sosmedSlice.actions;
export default sosmedSlice;
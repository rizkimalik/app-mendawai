import { createSlice } from '@reduxjs/toolkit'

const authSlice = createSlice({
    name: 'authSlice',
    initialState: {
        id: '',
        name: '',
        username: '',
        aux: '',
        aux_status: '',
        token: '',
        user_level: '',
        organization: '',
        department: '',
        email_address: '',
    },
    reducers: {
        setAuth: (state, action) => {
            state.id = action.payload.id;
            state.name = action.payload.name;
            state.username = action.payload.username;
            state.aux = action.payload.aux;
            state.aux_status = action.payload.aux_status;
            state.user_level = action.payload.user_level;
            state.organization = action.payload.organization;
            state.department = action.payload.department;
            state.email_address = action.payload.email_address;
            state.token = action.payload.token;
        }
    },
})

// dvalue from store in the slice file. For example: `useSelector((state) => state.counter.value)`
export const authUser = state => state.persistedReducer.authUser;

//export actions
export const { setAuth } = authSlice.actions;

//export reducer
export default authSlice;
import React, { useEffect } from 'react';
import { socket } from 'app/config';
import { useDispatch, useSelector } from 'react-redux';
import { ShowNotification } from 'views/components/Notification';
import { setSocketStatus } from 'app/slice/sliceSosmed';
import { authUser } from 'app/slice/sliceAuth';

const SocketIO = () => {
    const dispatch = useDispatch();
    const { username, user_level } = useSelector(authUser)

    useEffect(() => {
        if (user_level === 'L1') { //? socket connect user L1 only
            socket.auth = {
                flag_to: 'agent',
                username: username,
                email: ''
            }
            socket.connect();
            socket.on('connect', async function () {
                console.log(`connected: ${socket.connected}, id: ${socket.id}`);
                dispatch(setSocketStatus({
                    uuid: socket.id,
                    connected: socket.connected
                }))
            });
        }
    }, [dispatch, username, user_level]);

    useEffect(() => {
        socket.on('send-message-customer', (data) => { //push from blending
            ShowNotification(data);
        });
        socket.on('return-message-customer', (data) => {
            ShowNotification(data);
        });
        socket.on('return-message-whatsapp', (data) => {
            ShowNotification(data);
        });
        socket.on('return-directmessage-twitter', (data) => {
            ShowNotification(data);
        });

    }, [])

    return <></>;
}

export default SocketIO

import IconTicket from './IconTicket';
import IconMail from './IconMail';
import IconMailInbox from './IconMailInbox';
import IconMailSent from './IconMailSent';
import IconMailMarked from './IconMailMarked';
import IconMailSpam from './IconMailSpam';
import IconMailHistory from './IconMailHistory';
import IconMailOpen from './IconMailOpen';
import IconMailReply from './IconMailReply';
import IconCallInbound from './IconCallInbound';
import IconCallOutbound from './IconCallOutbound';
import IconCall from './IconCall';
import IconPlay from './IconPlay';
import IconClock from './IconClock';
import IconUserGroup from './IconUserGroup';
import IconGroupChat from './IconGroupChat';
import IconMention from './IconMention';
import IconLayoutPanel from './IconLayoutPanel';
import IconSetting from './IconSetting';
import IconMark from './IconMark';


export {
    IconTicket,
    IconMail,
    IconMailInbox,
    IconMailSent,
    IconMailMarked,
    IconMailSpam,
    IconMailHistory,
    IconMailOpen,
    IconMailReply,
    IconCallInbound,
    IconCallOutbound,
    IconCall,
    IconPlay,
    IconClock,
    IconUserGroup,
    IconGroupChat,
    IconMention,
    IconLayoutPanel,
    IconSetting,
    IconMark,
}
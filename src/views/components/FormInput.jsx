import React from 'react';
import FormGroup from "./FormGroup";

const FormInput = ({ register, name, type, className, label, placeholder, rules, readOnly, errors, list, datalist, formtext }) => {
    if (type === 'textarea') {
        return (
            <FormGroup label={label}>
                <textarea
                    name={name}
                    className={className}
                    placeholder={placeholder}
                    readOnly={readOnly}
                    cols="30"
                    rows="3"
                    {...register(name, { ...rules })}
                ></textarea>
                {errors && <span className="form-text text-danger">Please Enter {label}</span>}
            </FormGroup>
        )
    }
    else if (type === 'datalist') {
        return (
            <FormGroup label={label}>
                <input
                    name={name}
                    type={type}
                    list={list}
                    className={className}
                    placeholder={placeholder}
                    readOnly={readOnly}
                    {...register(name, { ...rules })}
                />
                <datalist id={list}>
                    {
                        datalist?.map((item, key) => {
                            return <option value={item} key={key}></option>;
                        })
                    }
                </datalist>
                {errors && <span className="form-text text-danger">Please Enter {label}</span>}
            </FormGroup>
        )
    }
    else if (type === 'text' || type === 'number' || type === 'email' || type === 'date') {
        return (
            <FormGroup label={label}>
                <input
                    name={name}
                    type={type}
                    className={className}
                    placeholder={placeholder}
                    readOnly={readOnly}
                    formtext={formtext}
                    {...register(name, { ...rules })}
                />
                {errors && <span className="form-text text-danger">Please Enter {label}</span>}
            </FormGroup>
        )
    }
    else if (type === 'checkbox') {
        return (
            <FormGroup label={label}>
                <div className="row">
                    <span className={className}>
                        <label>
                            <input
                                name={name}
                                type={type}
                                readOnly={readOnly}
                                {...register(name, { ...rules })}
                            />
                            <span />
                        </label>
                    </span>
                    {errors && <span className="form-text text-danger">Please Enter {label}</span>}
                </div>
            </FormGroup>
        )
    }
}

export default FormInput;
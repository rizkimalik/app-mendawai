import React from 'react'
import MendawaiLogo from './MendawaiLogo';

function SplashScreen() {
    return (
        <div id="splash-screen" className="splash-screen">
            <MendawaiLogo colorLogo="black" className="max-h-50px" />
            <span>Loading ...</span>
        </div>

    )
}

export default SplashScreen

import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import Icons from 'views/components/Icons'
import { SwalAlertError } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import { apiReportSentimentAnalysis, apiReportSentimentAnalysisExport } from 'app/services/apiReport';

function ReportSentimentAnalysis() {
    const dispatch = useDispatch();
    const { report_sentiment_analysis } = useSelector(state => state.report);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let now = new Date();
        const datetime = now.toISOString().slice(0, 10);
        setValue('date_start', datetime)
        setValue('date_end', datetime)
    }, [setValue]);

    const onSubmitFormReport = async (data) => {
        try {
            const result = await dispatch(apiReportSentimentAnalysis(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value
            }
            const result = await dispatch(apiReportSentimentAnalysisExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'Channel', key: 'channel' },
                    { header: 'Message', key: 'message' },
                    { header: 'Sentiment', key: 'sentiment' },
                    { header: 'Objective Score', key: 'objective_score' },
                    { header: 'Positive Score', key: 'positive_score' },
                    { header: 'Negative Score', key: 'negative_score' },
                    { header: 'Customer', key: 'name' },
                    { header: 'Datetime', key: 'date_create' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.csv.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Report_Sentiment_Analysis.csv');
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Sentiment Analysis" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Sentiment Analysis</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitFormReport)} id="formReportRangeDate">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_sentiment_analysis}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={false} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="Channel" dataField="channel" />
                    <Column caption="Message" dataField="message" width={400} cellRender={(data) => {
                        return <p className="text-wrap">{data.value}</p>
                    }} />
                    <Column caption="Sentiment" dataField="sentiment" cellRender={(data) => {
                        if (data.value === 'neutral') {
                            return <span className="text-warning">{data.value}</span>;
                        }
                        else if (data.value === 'positive') {
                            return <span className="text-success">{data.value}</span>;
                        } 
                        else if (data.value === 'negative') {
                            return <span className="text-danger">{data.value}</span>;
                        }
                    }} />
                    <Column caption="Objective Score" dataField="objective_score" />
                    <Column caption="Positive Score" dataField="positive_score" />
                    <Column caption="Negative Score" dataField="negative_score" />
                    <Column caption="Customer" dataField="name" />
                    <Column caption="Datetime" dataField="date_create" />
                </DataGrid>

            </Container>
        </MainContent>
    )
}

export default ReportSentimentAnalysis
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import Icons from 'views/components/Icons'
import { SwalAlertError } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import { apiReportOnProgress, apiReportOnProgressExport } from 'app/services/apiReport';

function ReportOnProgress() {
    const dispatch = useDispatch();
    const { report_onprogress } = useSelector(state => state.report);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let now = new Date();
        const datetime = now.toISOString().slice(0, 10);
        setValue('date_start', datetime)
        setValue('date_end', datetime)
    }, [setValue]);

    const onSubmitReportOnProgress = async (data) => {
        try {
            const result = await dispatch(apiReportOnProgress(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }

        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault()

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value
            }
            const result = await dispatch(apiReportOnProgressExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Group Ticket', key: 'group_ticket_number' },
                    { header: 'CustomerID', key: 'customer_id' },
                    { header: 'Channel', key: 'ticket_source' },
                    { header: 'Date Create', key: 'date_create' },
                    { header: 'Status', key: 'status' },
                    { header: 'Category', key: 'category_name' },
                    { header: 'Category Product', key: 'category_sublv1_name' },
                    { header: 'Category Case', key: 'category_sublv2_name' },
                    { header: 'Category Detail', key: 'category_sublv3_name' },
                    { header: 'SLA (Days)', key: 'sla' },
                    { header: 'SLA Status', key: 'sla_status' },
                    { header: 'Department', key: 'department_name' },
                    { header: 'Complaint Detail', key: 'complaint_detail' },
                    { header: 'Response Detail', key: 'response_detail' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Report_BaseOn_SLA.xlsx');
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Base on SLA" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report On Progress</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportOnProgress)} id="formReportOnProgress">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_onprogress}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Channel" dataField="ticket_source" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="User Created" dataField="user_create" />
                    <Column caption="Date Created" dataField="date_created" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="Department" dataField="department_name" />
                    <Column caption="Category" dataField="category_name" />
                    <Column caption="Category Product" dataField="category_sublv1_name" />
                    <Column caption="Category Case" dataField="category_sublv2_name" />
                    <Column caption="Category Detail" dataField="category_sublv3_name" />
                    <Column caption="SLA (Days)" dataField="sla" />
                    {/* <Column caption="SLA Status" dataField="sla_status" cellRender={(data) => {
                        return data.value === 'Over Due' ? <span className="text-danger">{data.value}</span> : <span className="text-success">{data.value}</span>;
                    }} /> */}
                    <Column caption="Dispatch Department" dataField="dispatch_to_department" />
                    <Column caption="Complaint Detail" dataField="complaint_detail" />
                    <Column caption="Response Detail" dataField="response_detail" />
                </DataGrid>

            </Container>
        </MainContent>
    )
}

export default ReportOnProgress
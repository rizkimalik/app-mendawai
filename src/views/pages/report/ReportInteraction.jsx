import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import Icons from 'views/components/Icons'
import { SwalAlertError } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import { apiReportInteraction, apiReportInteractionExport } from 'app/services/apiReport';

function ReportInteraction() {
    const dispatch = useDispatch();
    const { report_interaction } = useSelector(state => state.report);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let now = new Date();
        const datetime = now.toISOString().slice(0, 10);
        setValue('date_start', datetime)
        setValue('date_end', datetime)
    }, [setValue]);

    const onSubmitReportInteraction = async (data) => {
        try {
            const result = await dispatch(apiReportInteraction(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value
            }
            const result = await dispatch(apiReportInteractionExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');

                worksheet.columns = [
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Interaction ID', key: 'thread_id' },
                    { header: 'CustomerID', key: 'customer_id' },
                    { header: 'Channel', key: 'channel' },
                    { header: 'Status', key: 'status' },
                    { header: 'Date Created', key: 'created_at' },
                    { header: 'User Created', key: 'user_create' },
                    { header: 'User Response', key: 'response_complaint' },
                    { header: 'First Create', key: 'first_create' },
                    { header: 'Dispatch Ticket', key: 'dispatch_ticket' },
                    { header: 'Dispatch Layer', key: 'dispatch_to_layer' },
                    { header: 'Interaction Type', key: 'interaction_type' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Report_Interaction_Ticket.xlsx');
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Interaction Ticket" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Interaction Ticket</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportInteraction)} id="formReportInteraction">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_interaction}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" groupIndex={0} />
                    <Column caption="Interaction ID" dataField="thread_id" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Channel" dataField="channel" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="Date Created" dataField="created_at" />
                    <Column caption="User Created" dataField="user_create" />
                    <Column caption="User Response" dataField="response_complaint" />
                    <Column caption="First Create" dataField="first_create" />
                    <Column caption="Dispatch Ticket" dataField="dispatch_ticket" />
                    <Column caption="Dispatch Layer" dataField="dispatch_to_layer" />
                    <Column caption="Interaction Type" dataField="interaction_type" />
                </DataGrid>
            </Container>
        </MainContent>
    )
}

export default ReportInteraction
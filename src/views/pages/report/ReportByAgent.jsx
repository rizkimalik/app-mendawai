import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import Icons from 'views/components/Icons'
import { SwalAlertError } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import { apiReportByAgent, apiReportByAgent_Export } from 'app/services/apiReport';
import { authUser } from 'app/slice/sliceAuth';

function ReportByAgent() {
    const dispatch = useDispatch();
    const { report_by_agent } = useSelector(state => state.report);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();
    const user = useSelector(authUser);


    useEffect(() => {
        let now = new Date();
        const datetime = now.toISOString().slice(0, 10);
        setValue('date_start', datetime)
        setValue('date_end', datetime)
    }, [setValue]);

    const onSubmitReportByAgent = async (data) => {
        try {
            data.agent_handle = user.username;
            data.agent_layer = user.user_level;
            const result = await dispatch(apiReportByAgent(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value
            }
            const result = await dispatch(apiReportByAgent_Export(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');

                worksheet.columns = [
                    { header: 'User Created', key: 'agent_handle' },
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Interaction ID', key: 'thread_id' },
                    { header: 'CustomerID', key: 'customer_id' },
                    { header: 'Channel', key: 'ticket_source' },
                    { header: 'Channel Type', key: 'source_information' },
                    { header: 'Status', key: 'status' },
                    { header: 'Date Created', key: 'date_create' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Report_By_Agent.xlsx');
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Agent Ticket" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Ticket by Agent</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportByAgent)} id="formReportByAgent">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_by_agent}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="Agent Handle" dataField="user_create" />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="Interaction ID" dataField="thread_id" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Channel" dataField="ticket_source" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="Date Created" dataField="date_create" />
                </DataGrid>
            </Container>
        </MainContent>
    )
}

export default ReportByAgent
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { apiHistoryTransaction, apiLastResponseAgent, apiTicketShow } from 'app/services/apiTicket';
import Icons from 'views/components/Icons';
import { ButtonRefresh } from 'views/components/button';
import { TicketUpdate } from './index';
import { authUser } from 'app/slice/sliceAuth';

const TicketTransaction = () => {
    const dispatch = useDispatch();
    const [open_modal, setOpenModal] = useState('');
    const user = useSelector(authUser);
    const { selected_customer, history_transaction } = useSelector(state => state.ticket);
    const { customer_id } = selected_customer;

    useEffect(() => {
        if (customer_id) {
            dispatch(apiHistoryTransaction({ customer_id }))
        }
    }, [dispatch, customer_id]);

    function openModalTicketUpdate(data) {
        if (!data.data.last_response_by && user.user_level !== 'Admin') {
            dispatch(apiLastResponseAgent({ ticket_number: data.value, agent_handle: user.username }));
        }
        dispatch(apiTicketShow({ ticket_number: data.value }))
        setOpenModal('TicketUpdate');
    }

    return (
        <div className="border rounded p-4 my-2">
            <div className="d-flex justify-content-between mb-5">
                <h4>Ticket Journey</h4>
                <ButtonRefresh onClick={() => dispatch(apiHistoryTransaction({ customer_id }))} />
            </div>
            {open_modal === 'TicketUpdate' && <TicketUpdate />}
            <DataGrid
                dataSource={history_transaction}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    allowedPageSizes={[5, 10, 20]}
                    displayMode='full'
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Ticket Number" dataField="ticket_number" cellRender={(data) => {
                    return <button type="button" onClick={(e) => openModalTicketUpdate(data)} className="btn btn-sm btn-light-primary py-1 px-2" data-toggle="modal" data-target="#modalUpdateTicket">
                        <Icons iconName="ticket" className="svg-icon svg-icon-sm p-0" />
                        {data.value}
                    </button>
                }} />
                <Column caption="No. Reference" dataField="group_ticket_number" />
                <Column caption="Channel" dataField="ticket_source" />
                <Column caption="User Create" dataField="user_create" />
                <Column caption="Date Create" dataField="date_create" />
                <Column caption="Status" dataField="status" />
                <Column caption="Category" dataField="category_name" />
                <Column caption="Category Product" dataField="category_sublv1_name" />
                <Column caption="Category Case" dataField="category_sublv2_name" />
                <Column caption="Category Detail" dataField="category_sublv3_name" />
                <Column caption="SLA (Days)" dataField="sla" />
                <Column caption="Dispatch Department" dataField="dispatch_to_department" />
                <Column caption="Complaint Detail" dataField="complaint_detail" />
                <Column caption="Response Detail" dataField="response_detail" />
            </DataGrid>
        </div>
    )
}

export default TicketTransaction

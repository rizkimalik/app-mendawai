import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, Paging, Selection, Scrolling } from 'devextreme-react/data-grid'
import { apiMassDistruption } from 'app/services/apiTicket';
import { ButtonRefresh } from 'views/components/button';

function TicketReferenceModal({ setReferenceNo }) {
    const dispatch = useDispatch();
    const refCloseModal = useRef();
    const [selectedRow, setSelectedRow] = useState([]);
    const { mass_distruption } = useSelector(state => state.ticket);

    useEffect(() => {
        setSelectedRow([]);
        dispatch(apiMassDistruption());
    }, [dispatch]);

    function dataGridOnSelectionChanged(e) {
        setReferenceNo(e.selectedRowKeys[0].group_ticket_number);
        refCloseModal.current.click();
        setSelectedRow([]);
    }


    return (
        <div className="modal fade" id="modalTicketReference" tabIndex={-1} role="dialog" aria-labelledby="modalTicketReferenceLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalTicketReferenceLabel">Ticket Reference No.</h5>

                        <button type="button" ref={refCloseModal} className="close" data-toggle="modal" data-target="#modalTicketReference" aria-label="Close">
                            <i aria-hidden="true" className="ki ki-close"></i>
                        </button>
                    </div>
                    <div className="modal-body">
                        <span className="text-muted">Click row select data.</span>
                        <DataGrid
                            dataSource={mass_distruption}
                            height={300}
                            hoverStateEnabled={true}
                            onSelectionChanged={dataGridOnSelectionChanged}
                            selectedRowKeys={selectedRow}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <Selection mode="single" />
                            <Scrolling mode="virtual" />
                            <Paging enabled={true} pageSize={10} />
                            <FilterRow visible={true} />
                            <Column caption="Reference No" dataField="group_ticket_number" />
                            <Column caption="Category" dataField="category_name" />
                            <Column caption="Category Product" dataField="category_sublv1_name" />
                            <Column caption="Category Case" dataField="category_sublv2_name" />
                            <Column caption="Category Detail" dataField="category_sublv3_name" />
                        </DataGrid>
                    </div>
                    <div className="modal-footer">
                        <ButtonRefresh onClick={() => dispatch(apiMassDistruption())} />
                    </div>
                </div>
            </div>
        </div>

    )
}

export default TicketReferenceModal
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { NavLink, useLocation } from 'react-router-dom'

import Icons from 'views/components/Icons'
import FormGroup from 'views/components/FormGroup'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { ButtonRefresh, ButtonSubmit } from 'views/components/button'
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { apiCustomerList, apiCustomerShow, apiCustomerUpdate } from 'app/services/apiCustomer'
import { setSelectedCustomer } from 'app/slice/sliceTicket';
import CustomerJourney from 'views/pages/customer/CustomerJourney'
// import CustomerDataChannel from '../customer/CustomerDataChannel'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert'
import { IconMark, IconSetting, IconUserGroup } from 'views/components/icon'
import CustomerSyncronizeModal from '../customer/CustomerSyncronizeModal'
import FormInput from 'views/components/FormInput'

const TicketInformation = () => {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const location = useLocation();
    const [showModal, setShowModal] = useState('');
    const customer = useSelector(state => state.ticket.selected_customer);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        reset({});
        dispatch(setSelectedCustomer({})); //? reset cache data customer
    }, [dispatch, reset]);

    useEffect(() => {
        const customer_id = new URLSearchParams(search).get('customer_id');
        const account = new URLSearchParams(search).get('account');
        async function getShowCustomer() {
            try {
                if (customer.customer_id) {
                    const {
                        customer_id,
                        name,
                        email,
                        no_ktp,
                        birth,
                        gender,
                        phone_number,
                        address
                    } = customer;
                    reset({
                        customer_id,
                        name,
                        email,
                        no_ktp,
                        birth: birth?.slice(0, 10),
                        gender,
                        phone_number,
                        address
                    });
                    // dispatch(setSelectedCustomer(customer)); exist
                }
                else if (customer_id) {
                    const { payload } = await dispatch(apiCustomerShow({ customer_id }))
                    if (payload.data[0]) {
                        const {
                            customer_id,
                            name,
                            email,
                            no_ktp,
                            birth,
                            gender,
                            phone_number,
                            address
                        } = payload.data[0];
                        reset({
                            customer_id,
                            name,
                            email,
                            no_ktp,
                            birth: birth?.slice(0, 10),
                            gender,
                            phone_number,
                            address
                        });
                        dispatch(setSelectedCustomer(payload.data[0]));
                    }
                    else {
                        reset({});
                        SwalAlertError('Not Found', 'Data customer not found.');
                    }
                }
                else if (account) {
                    const { payload } = await dispatch(apiCustomerShow({ customer_id: account }))
                    if (payload.data[0]) {
                        const {
                            customer_id,
                            name,
                            email,
                            no_ktp,
                            birth,
                            gender,
                            phone_number,
                            address
                        } = payload.data[0];
                        reset({
                            customer_id,
                            name,
                            email,
                            no_ktp,
                            birth: birth?.slice(0, 10),
                            gender,
                            phone_number,
                            address
                        });
                        dispatch(setSelectedCustomer(payload.data[0]));
                    }
                    else {
                        reset({});
                        SwalAlertError('Not Found', 'Data customer not found.');
                    }
                }
            }
            catch (error) {
                reset({});
                SwalAlertError('Failed', error)
            }
        }
        getShowCustomer();
    }, [search, customer, reset, dispatch])

    const onSubmitUpdateCustomer = async (data) => {
        try {
            const { payload } = await dispatch(apiCustomerUpdate(data))
            if (payload.status === 200) {
                dispatch(setSelectedCustomer(payload.data));
                SwalAlertSuccess('Update Success', 'Success update data customer.');
            }
            else if (payload.status === 201) {
                SwalAlertError('Already Exists', payload.data);
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Failed', 'Please Try again.');
        }
    }

    return (
        <Card>
            <CardHeader>
                <CardTitle title="Customer Profile" subtitle="Detail Customer Information." />
                <CardToolbar>
                    <button onClick={(e) => setShowModal('CustomerList')} className="btn btn-icon btn-sm btn-light-info mx-2" title="Search Customer" data-toggle="modal" data-target="#modalListCustomer">
                        <Icons iconName="search" className="svg-icon svg-icon-md" />
                    </button>
                    <div className="dropdown dropdown-inline">
                        <button className="btn btn-sm btn-light-info btn-icon" data-toggle="dropdown" aria-expanded="false">
                            <IconSetting className="svg-icon svg-icon-md" />
                        </button>
                        <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                            <ul className="navi flex-column navi-hover py-2">
                                <li className="navi-header font-weight-bolder text-uppercase font-size-xs text-info pb-2"> Choose an action: </li>
                                <li className="navi-item">
                                    <div onClick={(e) => setShowModal('CustomerJourney')} data-toggle="modal" data-target="#modalJourneyCustomer" className="navi-link" style={{ cursor: 'pointer' }}>
                                        <i className="fas fa-route navi-icon"></i> Customer Journey
                                    </div>
                                </li>
                                <li className="navi-item">
                                    <NavLink to={`/customer/${customer.customer_id}/edit`} className="navi-link">
                                        <Icons iconName="user" className="navi-icon" /> Customer Detail
                                    </NavLink>
                                </li>
                                <li className="navi-item">
                                    <NavLink to="/customer" className="navi-link">
                                        <IconUserGroup className="navi-icon" /> Master Customers
                                    </NavLink>
                                </li>
                                <li className="navi-item">
                                    <div className="dropdown-divider"></div>
                                </li>
                                <li className="navi-item">
                                    <NavLink to={`${location.pathname}${location.search}`} onClick={(e) => setShowModal('CustomerSync')} className="navi-link" data-toggle="modal" data-target="#modalCustomerSyncronize">
                                        <Icons iconName="substract" className="navi-icon" /> Syncronize To
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </CardToolbar>
            </CardHeader>
            <CardBody className="p-4">
                <form onSubmit={handleSubmit(onSubmitUpdateCustomer)}>
                    <FormInput
                        name="customer_id"
                        type="text"
                        label="Customer ID"
                        className="form-control form-control-sm"
                        placeholder=""
                        formtext="Identity customer id"
                        register={register}
                        rules={{ required: true, maxLength: 100 }}
                        readOnly={true}
                        errors={errors.customer_id}
                    />
                    <FormInput
                        name="name"
                        type="text"
                        label="Full Name"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ required: true, maxLength: 100 }}
                        readOnly={false}
                        errors={errors.name}
                    />
                    <FormInput
                        name="phone_number"
                        type="number"
                        label="Phone Number"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ required: true, pattern: /^[0-9]+$/i }}
                        readOnly={false}
                        errors={errors.phone_number}
                    />
                    <FormInput
                        name="email"
                        type="email"
                        label="Email"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ required: true, pattern: /^\S+@\S+$/i }}
                        readOnly={false}
                        errors={errors.email}
                    />
                    <FormInput
                        name="no_ktp"
                        type="text"
                        label="Card ID"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ maxLength: 100 }}
                        readOnly={false}
                        errors={errors.no_ktp}
                    />
                    <FormGroup label="Gender">
                        <div className="radio-list mb-4">
                            <label className="radio">
                                <input type="radio" name="gender" value="Male" {...register("gender", { required: true })} />
                                <span />
                                Male
                            </label>
                            <label className="radio">
                                <input type="radio" name="gender" value="Female" {...register("gender", { required: true })} />
                                <span />
                                Female
                            </label>
                        </div>
                        {errors.gender && <span className="form-text text-danger">Please select gender</span>}
                    </FormGroup>
                    <FormInput
                        name="birth"
                        type="date"
                        label="Date of birth"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ maxLength: 10, required: true }}
                        readOnly={false}
                        errors={errors.birth}
                    />
                    <FormInput
                        name="address"
                        type="textarea"
                        label="Address"
                        className="form-control form-control-sm"
                        placeholder=""
                        register={register}
                        rules={{ required: true }}
                        readOnly={false}
                        errors={errors.address}
                    />
                    <div className="d-flex justify-content-between py-4">
                        <ButtonSubmit />
                    </div>
                </form>
                {/* <CustomerDataChannel customer_id={customer.customer_id} /> */}

                {showModal === 'CustomerList' && <ModalListCustomer />}
                {showModal === 'CustomerSync' && <CustomerSyncronizeModal customer={customer} />}
                {showModal === 'CustomerJourney' && <CustomerJourney customer={customer} />}
            </CardBody>
        </Card>
    )
}

export const ModalListCustomer = () => {
    const dispatch = useDispatch();
    const { customers } = useSelector(state => state.customer);

    useEffect(() => {
        dispatch(apiCustomerList())
    }, [dispatch]);

    function onSelectedCustomer(customer) {
        dispatch(setSelectedCustomer(customer));
    }

    function componentButtonActions(data) {
        const customer = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    data-dismiss="modal"
                    onClick={(e) => onSelectedCustomer(customer)}
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> {data.value}
                </button>
            </div>
        )
    }

    return (
        <Modal id="modalListCustomer">
            <ModalHeader title="Customer List" />
            <ModalBody>
                <DataGrid
                    dataSource={customers}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    {/* <Column caption="Actions" dataField="id" width={100} cellRender={componentButtonActions} /> */}
                    <Column caption="CustomerID" dataField="customer_id" cellRender={componentButtonActions} />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Email" dataField="email" />
                    <Column caption="Phone Number" dataField="phone_number" />
                    <Column caption="Card ID" dataField="no_ktp" />
                    <Column caption="Address" dataField="address" />
                    <Column caption="Status" dataField="status" cellRender={(data) => {
                        return <span className={`label label-md label-light-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                    }} />
                </DataGrid>
            </ModalBody>
            <ModalFooter>
                <ButtonRefresh onClick={() => dispatch(apiCustomerList())} />
            </ModalFooter>
        </Modal>
    )
}

export default TicketInformation

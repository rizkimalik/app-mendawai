import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'

import Icons from 'views/components/Icons'
import FormGroup from 'views/components/FormGroup'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { authUser } from 'app/slice/sliceAuth'
import { apiMasterDepartment, apiMasterChannel, apiMasterChannelType, apiMasterStatus, apiMasterUserLayer, } from 'app/services/apiMasterData'
import { apiHistoryTransaction, apiTicketStore } from 'app/services/apiTicket'
import { apiCategoryList, apiSubCategoryLv1, apiSubCategoryLv2, apiSubCategoryLv3, apiSubCategoryLv3Show } from 'app/services/apiCategory';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert'
import { TicketAttachment, TicketUserInteraction, TicketReferenceModal } from './index'
import { DatetimeLocal } from 'views/components/Datetime'

const TicketCreate = ({ customer }) => {
    const dispatch = useDispatch();
    const [navigate, setNavigate] = useState('tabFormCreateTicket');
    const [loading, setBtnLoading] = useState({ spinner: '', disabled: false });
    const [showBtnAction, setShowBtnAction] = useState(true);
    const [showBtnSearch, setShowBtnSearch] = useState(true);
    const [reference_no, setReferenceNo] = useState('');
    const { username, department } = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit, setValue, reset } = useForm();
    const { reporting_customer, ticket_thread } = useSelector(state => state.ticket);
    const { channels, channel_type, status, departments, user_layer } = useSelector(state => state.master);
    const { category, category_sublv1, category_sublv2, category_sublv3, category_sublv3_detail } = useSelector(state => state.category);

    useEffect(() => {
        //? load data master
        dispatch(apiMasterChannel())
        dispatch(apiMasterStatus())
        dispatch(apiMasterUserLayer())
        dispatch(apiCategoryList())
        dispatch(apiMasterDepartment())
    }, [dispatch]);

    useEffect(() => {
        setValue('ticket_position', 'L1')
        setValue('ticket_source', (ticket_thread?.thread_channel)?.split('_')[0])
        dispatch(apiMasterChannelType({ channel: (ticket_thread?.thread_channel)?.split('_')[0] })) //split value underscore
    }, [setValue, dispatch, ticket_thread]);

    useEffect(() => {
        setValue('sla', category_sublv3_detail?.sla)
        setValue('dispatch_department', category_sublv3_detail?.department_id)
    }, [setValue, category_sublv3_detail]);

    const onSubmitCreateTicket = async (data) => {
        setBtnLoading({ spinner: 'spinner spinner-white spinner-right', disabled: true });

        try {
            data.group_ticket_number = reference_no;
            const data_store = Object.assign({}, data, reporting_customer, ticket_thread); //? merge data ticket, reporting_customer,ticket_thread
            const { customer_id } = data_store;
            data_store.date_create = (data_store.date_create).replace('T', ' ');
            const { payload } = await dispatch(apiTicketStore(data_store));
            if (payload.status === 200) {
                SwalAlertSuccess('Ticket Created', 'Success into application!');
                dispatch(apiHistoryTransaction({ customer_id }))
                // dispatch(apiDataPublish({ customer_id }))
                setBtnLoading({ spinner: '', disabled: false })
                setShowBtnAction(false)
            }
            else {
                SwalAlertError('Create Failed', 'Please Try again.');
                setBtnLoading({ spinner: '', disabled: false })
            }
        } catch (error) {
            console.log(error)
            SwalAlertError('Create Failed', 'Please Try again.');
            setBtnLoading({ spinner: '', disabled: false })
        }
    }

    const onResetFormTicket = async () => {
        setBtnLoading({ spinner: '', disabled: false })
        setShowBtnAction(true);
        setReferenceNo('');
        reset();
    }

    const onCheckTicketType = async (e) => {
        setShowBtnSearch(e.target.checked ? false : true);
        setReferenceNo(e.target.checked ? reference_no : '');
    }

    return (
        <Modal id="modalCreateTicket">
            <ModalHeader title='Form Create Ticket' />
            <ModalBody>
                <div className="example-preview">
                    <ul className="nav nav-tabs" id="tablistCreateTicket" role="tablist">
                        <li className="nav-item">
                            <a onClick={() => setNavigate('tabFormCreateTicket')} className="nav-link active" id="tabFormCreateTicket" data-toggle="tab" href="#contentFormCreateTicket">
                                <span className="nav-icon">
                                    <Icons iconName="ticket" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Form Ticket</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={() => setNavigate('tabInteractionUser_FormCreate')} className="nav-link" id="tabInteractionUser_FormCreate" data-toggle="tab" href="#contentInteractionUser_FormCreate" aria-controls="contentInteractionUser_FormCreate">
                                <span className="nav-icon">
                                    <Icons iconName="group-chat" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Interaction User</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={() => setNavigate('tabAttachmentTicket_FormCreate')} className="nav-link" id="tabAttachmentTicket_FormCreate" data-toggle="tab" href="#contentAttachmentTicket_FormCreate" aria-controls="contentAttachmentTicket_FormCreate">
                                <span className="nav-icon">
                                    <Icons iconName="attachment" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Attachment Ticket</span>
                            </a>
                        </li>
                    </ul>

                    <div className="tab-content mt-5" id="contentCreateTicket" style={{ height: '530px' }}>
                        <div className="tab-pane fade active show" id="contentFormCreateTicket" role="tabpanel" aria-labelledby="tabFormCreateTicket">
                            <form onSubmit={handleSubmit(onSubmitCreateTicket)} id="formCreateTicket">
                                <input type="hidden" name="customer_id" value={customer.customer_id} {...register("customer_id", { required: true })} />
                                <input type="hidden" name="department_id" value={department} {...register("department_id", { required: true })} />
                                <input type="hidden" name="user_create" value={username} {...register("user_create", { required: true })} />
                                <div className="row">
                                    <div className="col-lg-3">
                                        <FormGroup label="Date Transaction">
                                            <input name="date_create" defaultValue={DatetimeLocal()}  {...register("date_create", { required: true })}
                                                type="datetime-local"
                                                className="form-control form-control-md"
                                            />
                                            {errors.date_create && <span className="form-text text-danger">Please select date</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-2">
                                        Mass Distrub
                                        <div className="form-group mt-2">
                                            <span className="switch switch-outline switch-icon switch-primary">
                                                <label>
                                                    <input type="checkbox" name="mass_distruption" onClick={(e) => onCheckTicketType(e)} defaultChecked={false} {...register("mass_distruption", { required: false })} />
                                                    <span />
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Reference No.">
                                            <div className="input-group">
                                                <input type="text" name="group_ticket_number" {...register("group_ticket_number", { required: false })}
                                                    defaultValue={reference_no} disabled={showBtnSearch} className="form-control" placeholder="Search..." />
                                                <div className="input-group-append">
                                                    <button onClick={() => setNavigate('modalTicketReference')} type="button" disabled={showBtnSearch} className="btn btn-light-primary" data-toggle="modal" data-target="#modalTicketReference">
                                                        <Icons iconName="search" className="svg-icon svg-icon-sm" />
                                                    </button>
                                                </div>
                                            </div>
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Ticket Status">
                                            <select name="status" className="form-control form-control-md" {...register("status", { required: true })}>
                                                <option value="">-- select status --</option>
                                                {
                                                    status.filter((item) => item.status === 'Open' || item.status === 'Closed').map((item, index) => {
                                                        return <option data-icon={`${item.icon} text-primary`} value={item.status} key={index}>{item.status}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.status && <span className="form-text text-danger">Please select status</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <hr />

                                <div className="row my-5">
                                    <div className="col-lg-3">
                                        <FormGroup label="Category">
                                            <select name="category_id"
                                                {...register("category_id", { required: true })}
                                                className="form-control form-control-md"
                                                onChange={(e) => dispatch(apiSubCategoryLv1({ category_id: e.target.value }))}
                                            >
                                                <option value="">-- select category --</option>
                                                {
                                                    category.map((item, index) => {
                                                        return <option value={item.category_id} key={index}>{item.name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_id && <span className="form-text text-danger">Please select category</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Product">
                                            <select name="category_sublv1_id"
                                                {...register("category_sublv1_id", { required: true })}
                                                className="form-control form-control-md"
                                                onChange={(e) => dispatch(apiSubCategoryLv2({ category_sublv1_id: e.target.value }))}
                                            >
                                                <option value="">-- select subcategory product --</option>
                                                {
                                                    category_sublv1?.map((item, index) => {
                                                        return <option value={item.category_sublv1_id} key={index}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv1_id && <span className="form-text text-danger">Please select subcategory product</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Case">
                                            <select name="category_sublv2_id"
                                                {...register("category_sublv2_id", { required: true })}
                                                className="form-control form-control-md"
                                                onChange={(e) => dispatch(apiSubCategoryLv3({ category_sublv2_id: e.target.value }))}
                                            >
                                                <option value="">-- select subcategory case --</option>
                                                {
                                                    category_sublv2?.map((item, index) => {
                                                        return <option value={item.category_sublv2_id} key={index}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv2_id && <span className="form-text text-danger">Please select subcategory case</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Detail">
                                            <select name="category_sublv3_id"
                                                {...register("category_sublv3_id", { required: true })}
                                                className="form-control form-control-md"
                                                onChange={(e) => dispatch(apiSubCategoryLv3Show({ category_sublv3_id: e.target.value }))}
                                            >
                                                <option value="">-- select subcategory detail --</option>
                                                {
                                                    category_sublv3?.map((item, index) => {
                                                        return <option value={item.category_sublv3_id} key={index}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv3_id && <span className="form-text text-danger">Please select subcategory detail</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row my-5">
                                    <div className="col-lg-2">
                                        <FormGroup label="Channel">
                                            <select name="ticket_source"
                                                className="form-control"
                                                {...register("ticket_source", { required: true })}
                                                onChange={(e) => dispatch(apiMasterChannelType({ channel: e.target.value }))}
                                            >
                                                <option value="">-- select channel --</option>
                                                {
                                                    channels.map((item, index) => {
                                                        let selected = item.channel === (ticket_thread?.thread_channel)?.split('_')[0] ? true : false;
                                                        return <option value={item.channel} key={index} selected={selected}>{item.channel}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.ticket_source && <span className="form-text text-danger">Please select channel</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-2">
                                        <FormGroup label="Channel Type">
                                            <select name="source_information" {...register("source_information", { required: true })} className="form-control form-control-md">
                                                <option value="">-- channel type --</option>
                                                {
                                                    channel_type?.map((item, index) => {
                                                        return <option value={item.channel_type} key={index}>{item.channel_type}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.source_information && <span className="form-text text-danger">Please select channel type</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Escalation to Layer">
                                            <select className="form-control" {...register("ticket_position", { required: true })}>
                                                {
                                                    user_layer.map((item, index) => {
                                                        return <option value={item.level_code} key={index} >{item.level_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.ticket_position && <span className="form-text text-danger">Please select layer</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Escalation to Department">
                                            <select name="dispatch_department" {...register("dispatch_department", { required: true })} className="form-control form-control-md">
                                                <option value="">-- select Department --</option>
                                                {
                                                    departments.map((item, index) => {
                                                        return <option value={item.id} key={index}>{item.department_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.dispatch_department && <span className="form-text text-danger">Please select Department</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-2">
                                        <FormGroup label="SLA (Days)">
                                            <input name="sla" type="number" {...register("sla", { required: true })} className="form-control form-control-md" readOnly />
                                            {errors.sla && <span className="form-text text-danger">Please enter SLA</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row my-5">
                                    <div className="col-lg-6">
                                        <FormGroup label="Complaint">
                                            <textarea name="complaint_detail" {...register("complaint_detail", { required: true })} className="form-control form-control-md" cols="10" rows="4"></textarea>
                                            {errors.complaint_detail && <span className="form-text text-danger">Please enter complaint</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-6">
                                        <FormGroup label="Response">
                                            <textarea name="response_detail" {...register("response_detail", { required: true })} className="form-control form-control-md" cols="10" rows="4"></textarea>
                                            {errors.response_detail && <span className="form-text text-danger">Please enter response</span>}
                                        </FormGroup>
                                    </div>
                                </div>

                                <ul className="nav nav-tabs nav-tabs-line hide">
                                    <li className="nav-item">
                                        <div className="nav-link active" data-toggle="tab">
                                            <span className="nav-text">+ Additional Data</span>
                                        </div>
                                    </li>
                                </ul>
                                <div className="row my-5 hide">
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 1">
                                            <input name="business_data1" {...register("business_data1", { required: false })}
                                                className="form-control form-control-md" />
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 2">
                                            <input name="business_data2" {...register("business_data2", { required: false })}
                                                className="form-control form-control-md" />
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 3">
                                            <input name="business_data3" {...register("business_data3", { required: false })}
                                                className="form-control form-control-md" />
                                        </FormGroup>
                                    </div>
                                </div>

                                <ModalFooter>
                                    {
                                        showBtnAction === true
                                            // ? <ButtonSubmit className="spinner spinner-white spinner-right" />
                                            ? <button type="submit" className={`btn btn-primary font-weight-bolder btn-sm m-1 ${loading.spinner}`} disabled={loading.disabled}>
                                                <Icons iconName="save" className="svg-icon svg-icon-sm" />
                                                Save changes
                                            </button>
                                            : <button onClick={() => onResetFormTicket()} type="button" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                                <Icons iconName="plus" className="svg-icon svg-icon-sm" />
                                                Add Transaction
                                            </button>
                                    }
                                </ModalFooter>
                            </form>
                        </div>
                        <div className="tab-pane fade" id="contentInteractionUser_FormCreate" role="tabpanel" aria-labelledby="tabInteractionUser_FormCreate">
                            {
                                navigate === 'tabInteractionUser_FormCreate' &&
                                <TicketUserInteraction
                                    interaction_id={ticket_thread.thread_id}
                                    customer_id={customer.customer_id}
                                    channel={ticket_thread.thread_channel}
                                    channel_type=""
                                />
                            }

                        </div>
                        <div className="tab-pane fade" id="contentAttachmentTicket_FormCreate" role="tabpanel" aria-labelledby="tabAttachmentTicket_FormCreate">
                            {
                                navigate === 'tabAttachmentTicket_FormCreate' &&
                                <TicketAttachment />
                            }
                        </div>
                    </div>
                </div>

                {/* popup window data ticket reference  */}
                {navigate === 'modalTicketReference' && <TicketReferenceModal setReferenceNo={setReferenceNo} />}
            </ModalBody>
        </Modal>
    )
}

export default React.memo(TicketCreate)

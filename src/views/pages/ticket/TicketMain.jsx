import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import Swal from 'sweetalert2'

import Icons from 'views/components/Icons'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { authUser } from 'app/slice/sliceAuth'
import {
    TicketTransaction,
    TicketThread,
    TicketReporting,
    TicketInformation,
    TicketCreate,
} from './index'
import { IconCall, IconGroupChat, IconMailHistory } from 'views/components/icon'

const TicketMain = () => {
    const { user_level } = useSelector(authUser);
    const [open_modal, setOpenModal] = useState('');
    const { selected_customer, reporting_customer, ticket_thread } = useSelector(state => state.ticket);
    const customer = selected_customer;

    function onCheckValidation() {
        setOpenModal('TicketCreate');
        if (!customer.customer_id) {
            Swal.fire({
                title: "Empty Customer.",
                text: "Please select a customer!",
                buttonsStyling: false,
                icon: "warning",
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-primary"
                },
            })
        }
        else if (customer.status !== 'Registered') {
            Swal.fire({
                title: "Invalid Customer.",
                text: "Customer data not Registered!",
                buttonsStyling: false,
                icon: "warning",
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-primary"
                },
            })
        }
        else if (!reporting_customer.cust_email) {
            Swal.fire({
                title: "Data Reported Empty.",
                text: "Please enter data reported",
                buttonsStyling: false,
                icon: "warning",
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-primary"
                },
            })
        }
        else if (!ticket_thread.thread_id) {
            Swal.fire({
                title: "Interaction ID Empty",
                text: "Please enter interaction id.",
                buttonsStyling: false,
                icon: "warning",
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-primary"
                },
            })
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Ticket" menu_name="Main Ticket" modul_name="">
                <NavLink to="/omnichannel/call" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconCall className="svg-icon svg-icon-sm" /> Data Call
                </NavLink>
                <NavLink to="/omnichannel/email" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconMailHistory className="svg-icon svg-icon-sm" /> Data Mailbox
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconGroupChat className="svg-icon svg-icon-sm" /> Data Sosial Media
                </NavLink>
            </SubHeader>
            <Container>
                {
                    //? validation data customer
                    customer.customer_id &&
                    reporting_customer.cust_email &&
                    ticket_thread.thread_id &&
                    customer.status === 'Registered' &&
                    open_modal === 'TicketCreate' &&
                    <TicketCreate customer={customer} />
                }
                <main className="row">
                    <section className="col-lg-8 pl-1">
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle title="Ticket Detail" subtitle="Information Customer Detail." />
                                <CardToolbar>
                                    {
                                        user_level === 'L1' &&
                                        <button type="button" onClick={onCheckValidation} className="btn btn-sm btn-primary font-weight-bolder" title="Create Ticket" data-toggle="modal" data-target="#modalCreateTicket">
                                            <Icons iconName="plus" className="svg-icon svg-icon" />
                                            Create Ticket
                                        </button>
                                    }
                                </CardToolbar>
                            </CardHeader>
                            <CardBody className="p-4">
                                <TicketThread customer={customer} /> {/* Channel Interaction */}
                                <TicketReporting /> {/* Data Reported */}
                                <TicketTransaction /> {/* Ticket Journey */}
                            </CardBody>
                        </Card>
                    </section>
                    <section className="col-lg-4">
                        <TicketInformation /> {/* Customer Information */}
                    </section>
                </main>
            </Container>
        </MainContent>
    )
}

export default TicketMain

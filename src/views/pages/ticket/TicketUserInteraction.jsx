import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { apiInteractionUser } from 'app/services/apiTicket';
import Attachment from '../channel/socmed/Attachment';
import { getPabxRecording, getPabxToken } from 'app/services/apiSosmed';

const TicketUserInteraction = ({ interaction_id, customer_id, channel, channel_type }) => {
    const dispatch = useDispatch();
    const [pbx_recording, setPbxRecording] = useState('');
    const { interaction_user } = useSelector(state => state.ticket);

    useEffect(() => {
        dispatch(apiInteractionUser({
            interaction_id,
            customer_id,
            channel,
            channel_type
        }));
    }, [dispatch, interaction_id, customer_id, channel, channel_type]);

    async function playCallDetailRecording(data) {
        setPbxRecording('');
        const token = await getPabxToken();
        if (token.status === 200) {
            const result = await getPabxRecording({ token: token.data.access_token, interaction_id: data.interaction_id });
            if (result) {
                setPbxRecording(`${token.data.api_url}/files/${result.file_id}/data`)
            }
            else {
                setPbxRecording('');
                SwalAlertError('Play Failed', 'File not found.');
            }
        }
        else {
            setPbxRecording('');
            SwalAlertError(token.message, token.data);
        }
    }

    if (channel === 'Email') {
        return <LayoutInteraction><DataEmailInteraction data={interaction_user} /></LayoutInteraction>
    }
    else if (channel === 'Chat') {
        return <LayoutInteraction><DataAllMessages data={interaction_user} /></LayoutInteraction>
    }
    else if (channel === 'Whatsapp') {
        return <LayoutInteraction><DataAllMessages data={interaction_user} /></LayoutInteraction>
    }
    else if (channel === 'Twitter') {
        return <LayoutInteraction><DataAllMessages data={interaction_user} /></LayoutInteraction>
    }
    else if (channel === 'Facebook') {
        return <LayoutInteraction><DataAllMessages data={interaction_user} /></LayoutInteraction>
    }
    else if (channel === 'Call') {
        return <LayoutInteraction>
            <DataCallInteraction data={interaction_user} playCallDetailRecording={playCallDetailRecording} />
            {
                pbx_recording &&
                <video controls width="100%" height={420} className="border rounded">
                    <source src={pbx_recording} type="video/mp4" />
                </video>
            }
        </LayoutInteraction>
    }
    else {
        return <LayoutInteraction>
            <div className="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
                <div className="alert-icon"><i className="flaticon-warning" /></div>
                <div className="alert-text">Interaction not found.</div>
            </div>
        </LayoutInteraction>
    }
}

function DataEmailInteraction({ data }) {
    if (data[0]) {
        return <div className="cursor-pointer toggle-on" data-inbox="message">
            <div className="d-flex p-5 flex-row flex-md-row flex-lg-row flex-xxl-row justify-content-between">
                <div className="d-flex align-items-center">
                    <span className="symbol symbol-50 mr-4 border">
                        <span className="symbol-label">
                            <i className="fa fa-envelope text-primary"></i>
                        </span>
                    </span>
                    <div className="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                        <div className="d-flex">
                            <span className="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">{data[0].ESUBJECT}</span>
                        </div>
                        <div className="d-flex flex-column">
                            <div className="toggle-off-item">
                                <span className="font-weight-bold text-muted cursor-pointer">
                                    From: {data[0].EFROM}
                                </span>
                            </div>
                        </div>
                        <div className="d-flex flex-column">
                            <div className="toggle-off-item">
                                <span className="font-weight-bold text-muted cursor-pointer">
                                    To: {data[0].ETO}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-center align-items-xxl-center flex-row flex-md-row flex-lg-row flex-xxl-row">
                    <div className="font-weight-bold text-muted mx-2">{(data[0].Email_Date).replace('T', ' ').substring(19, 0)}</div>
                    <div className="d-flex align-items-center flex-wrap flex-xxl-nowrap" data-inbox="toolbar">
                        <span className="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="Settings">
                            <i className="flaticon-more icon-1x" />
                        </span>
                    </div>
                </div>
            </div>
            <div className="p-5" dangerouslySetInnerHTML={{ __html: data[0].EBODY_HTML }}></div>
        </div>
    }
    else {
        return <div className="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
            <div className="alert-icon"><i className="flaticon-warning" /></div>
            <div className="alert-text">Interaction not found.</div>
        </div>
    }
}

function DataCallInteraction({ data, playCallDetailRecording }) {
    if (data.length > 0) {
        return (
            <div className="list border rounded mb-4">
                <div className='list-item d-flex align-items-center justify-content-between'>
                    <div className="d-flex align-items-center py-2 mx-2">
                        <span className="bullet bullet-bar bg-danger align-self-stretch mr-2"></span>
                        <div className="symbol symbol-40px">
                            <div className="symbol-label">
                                <i className="fas fa-phone text-danger"></i>
                            </div>
                        </div>
                        <div className="flex-grow-1 mx-2">
                            <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">{data[0].interaction_id}</div>
                            <div className="mt-2">
                                <span className="text-mute m-2">Caller {data[0].caller}</span>
                                <span className="text-mute"> - </span>
                                <span className="text-mute m-2">Callee {data[0].callee}</span>
                            </div>
                        </div>
                        <div className="flex-grow-1 mx-2">
                            <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">customer - {data[0].customer_id}</div>
                            <div className="mt-2">
                                <span className="text-mute m-2">Phone Number</span>
                                <span className="text-mute"> - </span>
                                <span className="text-mute m-2">{data[0].phone_number}</span>
                            </div>
                        </div>
                        <div className="flex-grow-1 mx-2">
                            <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Answered by {data[0].agent_handle}</div>
                            <div className="mt-2">
                                <span className="text-mute m-2">{data[0].start_time}</span>
                                <span className="text-mute"> - </span>
                                <span className="text-mute m-2">{data[0].ended_time}</span>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex-column align-items-end mx-2">
                        <button onClick={(e) => playCallDetailRecording(data[0])} className="btn btn-light-primary font-weight-bold">Play Recording</button>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return <div className="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
            <div className="alert-icon"><i className="flaticon-warning" /></div>
            <div className="alert-text">Interaction not found.</div>
        </div>
    }
}

//chat, whatsapp, messanger, twitter_dm, instagram_dm
function DataAllMessages({ data }) {
    if (data?.length > 0) {
        return <div className="messages p-8">
            {
                data?.map((item, index) => {
                    if (item.flag_to === 'customer') {
                        return (
                            <div className="d-flex justify-content-start mb-10" key={index}>
                                <div className="d-flex flex-column align-items-start">
                                    <div className="d-flex align-items-center mb-2">
                                        <div className="ml-3">
                                            <span className="fs-5 fw-bolder text-gray-900 text-hover-primary">{item.name}</span>
                                            <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                        </div>
                                    </div>
                                    <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                        {item.attachment && <Attachment value={item.attachment} />}
                                        {item.message}
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    else if (item.flag_to === 'agent') {
                        return (
                            <div className="d-flex justify-content-end mb-10" key={index}>
                                <div className="d-flex flex-column align-items-end">
                                    <div className="d-flex align-items-center mb-2">
                                        <div className="mr-3">
                                            <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                            <span className="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">{item.name}</span>
                                        </div>
                                    </div>
                                    <div className="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">
                                        {item.attachment && <Attachment value={item.attachment} />}
                                        {item.message}
                                    </div>
                                </div>
                            </div>
                        )
                    }

                    return ('');
                })
            }
        </div>
    }
    else {
        return <div className="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
            <div className="alert-icon"><i className="flaticon-warning" /></div>
            <div className="alert-text">Interaction not found.</div>
        </div>
    }
}

function LayoutInteraction({ children }) {
    return <div className="p-0">
        <div className="row">
            <div className="col-lg-12">
                <div style={{ height: '500px', overflow: 'auto' }}>
                    {children}
                </div>
            </div>
        </div>
    </div>
}

export default TicketUserInteraction;

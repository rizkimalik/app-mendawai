import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'

import Icons from 'views/components/Icons'
import { ButtonSubmit } from 'views/components/button'
import FormGroup from 'views/components/FormGroup'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { authUser } from 'app/slice/sliceAuth'
import { apiMasterDepartment, apiMasterChannel, apiMasterChannelType, apiMasterStatus, apiMasterUserLayer } from 'app/services/apiMasterData'
import { apiHistoryTransaction, apiTicketShow, apiTicketUpdate } from 'app/services/apiTicket'
import {
    apiCategoryList,
    apiSubCategoryLv1,
    apiSubCategoryLv2,
    apiSubCategoryLv3,
    apiSubCategoryLv3Show
} from 'app/services/apiCategory';
import {
    TicketInteraction,
    TicketAttachment,
    TicketUserInteraction,
    TicketReference
} from './index'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert'

const TicketUpdate = () => {
    const dispatch = useDispatch();
    const [navigate, setNavigate] = useState('tabFormUpdateTicket');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { username, user_level } = useSelector(authUser)
    const { channels, channel_type, status, departments, user_layer } = useSelector(state => state.master);
    const { ticket } = useSelector(state => state.ticket);
    const { category, category_sublv1, category_sublv2, category_sublv3 } = useSelector(state => state.category);


    useEffect(() => {
        dispatch(apiMasterChannel())
        dispatch(apiMasterStatus())
        dispatch(apiMasterUserLayer())
        dispatch(apiMasterDepartment())
        dispatch(apiCategoryList())
    }, [dispatch]);

    useEffect(() => {
        dispatch(apiSubCategoryLv1({ category_id: ticket.category_id }))
        dispatch(apiSubCategoryLv2({ category_sublv1_id: ticket.category_sublv1_id }))
        dispatch(apiSubCategoryLv3({ category_sublv2_id: ticket.category_sublv2_id }))
        dispatch(apiSubCategoryLv3Show({ category_sublv3_id: ticket.category_sublv3_id }))
        dispatch(apiMasterChannelType({ channel: ticket.ticket_source }))
    }, [dispatch, ticket]);

    useEffect(() => {
        setTimeout(() => {
            reset({
                // user_create: ticket.user_create,
                customer_id: ticket.customer_id,
                date_create: ticket.date_create,
                ticket_source: ticket.ticket_source,
                ticket_number: ticket.ticket_number,
                thread_id: ticket.thread_id,
                status: ticket.status,
                category_id: ticket.category_id,
                category_sublv1_id: ticket.category_sublv1_id,
                category_sublv2_id: ticket.category_sublv2_id,
                category_sublv3_id: ticket.category_sublv3_id,
                complaint_detail: ticket.complaint_detail,
                response_detail: ticket.response_detail,
                sla: ticket.sla,
                ticket_position: ticket.ticket_position,
                dispatch_department: ticket.dispatch_department,
                department_id: ticket.department_id,
                mass_distruption: ticket.mass_distruption,
                group_ticket_number: ticket.group_ticket_number,
                type_complaint: ticket.type_complaint,
                source_information: ticket.source_information,
                business_data1: ticket.business_data1,
                business_data2: ticket.business_data2,
                business_data3: ticket.business_data3,
            });
        }, 2000);

    }, [reset, ticket]);

    const onSubmitUpdateTicket = async (data) => {
        try {
            const { customer_id } = data;
            data.date_create = (data.date_create).replace('T', ' '); //? format datetime
            const { payload } = await dispatch(apiTicketUpdate(data));
            if (payload.status === 200) {
                SwalAlertSuccess('Ticket Update', 'Success update Ticket!');
                dispatch(apiHistoryTransaction({ customer_id }))
                dispatch(apiTicketShow({ ticket_number: data.ticket_number }))
            }
            else {
                SwalAlertError('Create Failed', 'Please Try again.');
            }
        } catch (error) {
            console.log(error)
            SwalAlertError('Create Failed', 'Please Try again.');
        }
    }

    return (
        <Modal id="modalUpdateTicket">
            <ModalHeader title={`Form Update Ticket - #${ticket.ticket_number}`} />
            <ModalBody>
                <div className="example-preview">
                    <ul className="nav nav-tabs" id="tablistUpdateTicket" role="tablist">
                        <li className="nav-item">
                            <a onClick={(e) => setNavigate('tabFormUpdateTicket')} className="nav-link active" id="tabFormUpdateTicket" data-toggle="tab" href="#contentFormUpdateTicket">
                                <span className="nav-icon">
                                    <Icons iconName="ticket" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Form Update Ticket</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={(e) => setNavigate('tabInteractionTicket')} className="nav-link" id="tabInteractionTicket" data-toggle="tab" href="#contentInteractionTicket" aria-controls="contentInteractionTicket">
                                <span className="nav-icon">
                                    <Icons iconName="substract" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Interaction Ticket</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={(e) => setNavigate('tabReferenceTicket')} className="nav-link" id="tabReferenceTicket" data-toggle="tab" href="#contentReferenceTicket" aria-controls="contentReferenceTicket">
                                <span className="nav-icon">
                                    <Icons iconName="layer" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">References Ticket</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={(e) => setNavigate('tabInteractionUser')} className="nav-link" id="tabInteractionUser" data-toggle="tab" href="#contentInteractionUser" aria-controls="contentInteractionUser">
                                <span className="nav-icon">
                                    <Icons iconName="group-chat" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Interaction User</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={(e) => setNavigate('tabAttachmentTicket')} className="nav-link" id="tabAttachmentTicket" data-toggle="tab" href="#contentAttachmentTicket" aria-controls="contentAttachmentTicket">
                                <span className="nav-icon">
                                    <Icons iconName="attachment" className="svg-icon svg-icon-sm" />
                                </span>
                                <span className="nav-text">Attachment Ticket</span>
                            </a>
                        </li>
                    </ul>

                    <div className="tab-content mt-5" id="contentUpdateTicket" style={{ height: '520px' }}>
                        <div className="tab-pane fade active show" id="contentFormUpdateTicket" role="tabpanel" aria-labelledby="tabFormUpdateTicket">
                            <form onSubmit={handleSubmit(onSubmitUpdateTicket)} id="formUpdateTicket">
                                <input type="hidden" {...register("customer_id", { required: true })} />
                                <input type="hidden" {...register("ticket_number", { required: true })} />
                                <input type="hidden" {...register("thread_id", { required: true })} />
                                <input type="hidden" {...register("department_id", { required: true })} />
                                <input type="hidden" value={username} {...register("user_create", { required: true })} />
                                <div className="row">
                                    <div className="col-lg-3">
                                        <FormGroup label="Date Transaction">
                                            <input {...register("date_create", { required: true })}
                                                type="datetime-local" className="form-control form-control-md" disabled
                                            />
                                            {errors.date_create && <span className="form-text text-danger">Please select date</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-1"></div>
                                    <div className="col-lg-2">
                                        Mass Distrub
                                        <div className="form-group mt-2">
                                            <span className="switch switch-outline switch-icon switch-primary">
                                                <label>
                                                    <input type="checkbox" name="mass_distruption" defaultChecked={false} {...register("mass_distruption", { required: false })} disabled />
                                                    <span />
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Reference No.">
                                            <input type="text" name="group_ticket_number" {...register("group_ticket_number", { required: false })}
                                                className="form-control form-control-md" disabled />
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Ticket Status">
                                            <select className="form-control" {...register("status", { required: true })}>
                                                <option value="">-- select status --</option>
                                                {
                                                    status.map((item, index) => {
                                                        return <option data-icon={`${item.icon} text-primary`} value={item.status} key={index}>{item.status}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.status && <span className="form-text text-danger">Please select status</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <hr />

                                <div className="row my-5">
                                    <div className="col-lg-3">
                                        <FormGroup label="Category">
                                            <select
                                                {...register("category_id", { required: true })}
                                                className="form-control form-control-md"
                                                // onChange={(e) => dispatch(apiSubCategoryLv1({ category_id: e.target.value }))}
                                                disabled
                                            >
                                                <option value="">-- select category --</option>
                                                {
                                                    category.map((item, index) => {
                                                        return <option value={item.category_id} key={index}>{item.name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_id && <span className="form-text text-danger">Please select category</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Product">
                                            <select
                                                {...register("category_sublv1_id", { required: true })}
                                                className="form-control form-control-md"
                                                // onChange={(e) => dispatch(apiSubCategoryLv2({ category_sublv1_id: e.target.value }))}
                                                disabled
                                            >
                                                <option value="">-- select subcategory product --</option>
                                                {
                                                    category_sublv1?.map((item, index) => {
                                                        let selected = item.category_sublv1_id === ticket.category_sublv1_id ? true : false;
                                                        return <option value={item.category_sublv1_id} key={index} selected={selected}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv1_id && <span className="form-text text-danger">Please select subcategory product</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Case">
                                            <select
                                                {...register("category_sublv2_id", { required: true })}
                                                className="form-control form-control-md"
                                                // onChange={(e) => dispatch(apiSubCategoryLv3({ category_sublv2_id: e.target.value }))}
                                                disabled
                                            >
                                                <option value="">-- select subcategory case --</option>
                                                {
                                                    category_sublv2?.map((item, index) => {
                                                        let selected = item.category_sublv2_id === ticket.category_sublv2_id ? true : false;
                                                        return <option value={item.category_sublv2_id} key={index} selected={selected}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv2_id && <span className="form-text text-danger">Please select subcategory case</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Category Detail">
                                            <select
                                                {...register("category_sublv3_id", { required: true })}
                                                className="form-control form-control-md"
                                                // onChange={(e) => dispatch(apiSubCategoryLv3Show({ category_sublv3_id: e.target.value }))}
                                                disabled
                                            >
                                                <option value="">-- select subcategory detail --</option>
                                                {
                                                    category_sublv3?.map((item, index) => {
                                                        let selected = item.category_sublv3_id === ticket.category_sublv3_id ? true : false;
                                                        return <option value={item.category_sublv3_id} key={index} selected={selected}>{item.sub_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.category_sublv3_id && <span className="form-text text-danger">Please select subcategory detail</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row my-5">
                                    <div className="col-lg-2">
                                        <FormGroup label="Channel">
                                            <select className="form-control form-control-md" {...register("ticket_source", { required: true })} disabled>
                                                <option value="">-- select channel --</option>
                                                {
                                                    channels.map((item, index) => {
                                                        let selected = item.channel === ticket.ticket_source ? true : false;
                                                        return <option value={item.channel} key={index} selected={selected}>{item.channel}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.ticket_source && <span className="form-text text-danger">Please select channel</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-2">
                                        <FormGroup label="Channel Type">
                                            <select {...register("source_information", { required: true })} className="form-control form-control-md" disabled>
                                                <option value="">-- select channel type --</option>
                                                {
                                                    channel_type?.map((item, index) => {
                                                        let selected = item.channel_type === ticket.source_information ? true : false;
                                                        return <option value={item.channel_type} key={index} selected={selected}>{item.channel_type}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.source_information && <span className="form-text text-danger">Please select channel type</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Escalation to Layer">
                                            <select className="form-control" {...register("ticket_position", { required: true })}>
                                                {
                                                    user_layer.map((layer, index) => {
                                                        return <option value={layer.level_code} key={index} >{layer.level_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.ticket_position && <span className="form-text text-danger">Please select layer</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-3">
                                        <FormGroup label="Escalation to Department">
                                            <select {...register("dispatch_department", { required: true })} className="form-control form-control-md">
                                                <option value="">-- select Department --</option>
                                                {
                                                    departments.map((item, index) => {
                                                        return <option value={item.id} key={index}>{item.department_name}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.dispatch_department && <span className="form-text text-danger">Please select Department</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-2">
                                        <FormGroup label="SLA (Days)">
                                            <input type="number" {...register("sla", { required: true })} className="form-control form-control-md" disabled />
                                            {errors.sla && <span className="form-text text-danger">Please enter SLA</span>}
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row my-5">
                                    <div className="col-lg-6">
                                        <FormGroup label="Complaint">
                                            <textarea {...register("complaint_detail", { required: true })} className="form-control form-control-md" cols="10" rows="4" disabled></textarea>
                                            {errors.complaint_detail && <span className="form-text text-danger">Please enter complaint</span>}
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-6">
                                        <FormGroup label="Response">
                                            <textarea {...register("response_detail", { required: true })} className="form-control form-control-md" cols="10" rows="4"></textarea>
                                            {errors.response_detail && <span className="form-text text-danger">Please enter response</span>}
                                        </FormGroup>
                                    </div>
                                </div>

                                <ul className="nav nav-tabs nav-tabs-line hide">
                                    <li className="nav-item">
                                        <div className="nav-link active" data-toggle="tab">
                                            <span className="nav-text">+ Additional Data</span>
                                        </div>
                                    </li>
                                </ul>
                                <div className="row my-5 hide">
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 1">
                                            <input name="business_data1" {...register("business_data1", { required: false })}
                                                className="form-control form-control-md" disabled />
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 2">
                                            <input name="business_data2" {...register("business_data2", { required: false })}
                                                className="form-control form-control-md" disabled />
                                        </FormGroup>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormGroup label="Business Data 3">
                                            <input name="business_data3" {...register("business_data3", { required: false })}
                                                className="form-control form-control-md" disabled />
                                        </FormGroup>
                                    </div>
                                </div>

                                <ModalFooter>
                                    {
                                        ticket.status !== 'Closed'
                                        // && user_level.substring(user_level.length - 1) === ticket.ticket_position
                                        && user_level === ticket.ticket_position
                                        && ticket.last_response_by === username
                                        && <ButtonSubmit />
                                    }
                                </ModalFooter>
                            </form>
                        </div>
                        <div className="tab-pane fade" id="contentInteractionTicket" role="tabpanel" aria-labelledby="tabInteractionTicket">
                            {navigate === 'tabInteractionTicket' && <TicketInteraction />}
                        </div>
                        <div className="tab-pane fade" id="contentReferenceTicket" role="tabpanel" aria-labelledby="tabReferenceTicket">
                            {navigate === 'tabReferenceTicket' && <TicketReference ticket={ticket} />}
                        </div>
                        <div className="tab-pane fade" id="contentInteractionUser" role="tabpanel" aria-labelledby="tabInteractionUser">
                            {
                                navigate === 'tabInteractionUser' &&
                                <TicketUserInteraction
                                    interaction_id={ticket.thread_id}
                                    customer_id={ticket.customer_id}
                                    channel={ticket.ticket_source}
                                    channel_type={ticket.source_information}
                                />
                            }
                        </div>
                        <div className="tab-pane fade" id="contentAttachmentTicket" role="tabpanel" aria-labelledby="tabAttachmentTicket">
                            {navigate === 'tabAttachmentTicket' && <TicketAttachment />}
                        </div>
                    </div>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default React.memo(TicketUpdate)

import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';

import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import Icons from 'views/components/Icons'
import FormGroup from 'views/components/FormGroup'
import { apiHistoryTicket, apiHistoryTicketExport, apiTicketShow } from 'app/services/apiTicket'
import { SwalAlertError } from 'views/components/SwalAlert';
import { TicketUpdate } from '.';

const TicketHistory = () => {
    const dispatch = useDispatch();
    const [action, setAction] = useState('');
    const { history_ticket } = useSelector(state => state.ticket);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let now = new Date();
        const datetime = now.toISOString().slice(0, 10);
        setValue('date_start', datetime)
        setValue('date_end', datetime)
    }, [setValue]);

    const onSubmitHistoryTicket = async (data) => {
        try {
            const result = await dispatch(apiHistoryTicket(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value
            }
            const result = await dispatch(apiHistoryTicketExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Group Ticket', key: 'group_ticket_number' },
                    { header: 'CustomerID', key: 'customer_id' },
                    { header: 'Channel', key: 'ticket_source' },
                    { header: 'Date Create', key: 'date_create' },
                    { header: 'Status', key: 'status' },
                    { header: 'Category', key: 'category_name' },
                    { header: 'Category Product', key: 'category_sublv1_name' },
                    { header: 'Category Case', key: 'category_sublv2_name' },
                    { header: 'Category Detail', key: 'category_sublv3_name' },
                    { header: 'SLA (Days)', key: 'sla' },
                    { header: 'Department', key: 'department_name' },
                    { header: 'Complaint Detail', key: 'complaint_detail' },
                    { header: 'Response Detail', key: 'response_detail' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'ExcelHistoryTicketGrid.xlsx');
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }

    }

    const actionButtonGrid = async (data) => {
        dispatch(apiTicketShow({ ticket_number: data.value }))
        setAction('modalTicketUpdate');
    }

    return (
        <MainContent>
            <SubHeader active_page="Ticket History" menu_name="Ticket" modul_name="Ticket History" />
            <Container>
                {
                    action === 'modalTicketUpdate' &&
                    <TicketUpdate />
                }

                <div className="border rounded p-4 my-4">
                    <div className="d-flex align-items-center justify-content-between">
                        <h4>Ticket History</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitHistoryTicket)} id="formHistoryTicket">
                        <div className="row">
                            <div className="col-lg-2 m-0">
                                <FormGroup label="Date From">
                                    <input type="date" {...register("date_start", { required: true })} className="form-control form-control-sm" />
                                    {errors.date_start && <span className="form-text text-danger">Please select date</span>}
                                </FormGroup>
                            </div>
                            <div className="col-lg-2">
                                <FormGroup label="Date To">
                                    <input type="date" {...register("date_end", { required: true })} className="form-control form-control-sm" />
                                    {errors.date_end && <span className="form-text text-danger">Please select date</span>}
                                </FormGroup>
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={history_ticket}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" fixed={true} cellRender={(data) => {
                        return <button type="button" onClick={(e) => actionButtonGrid(data)} className="btn btn-sm btn-light-primary py-1 px-2" data-toggle="modal" data-target="#modalUpdateTicket">
                            <Icons iconName="ticket" className="svg-icon svg-icon-sm p-0" />
                            {data.value}
                        </button>
                    }} />
                    <Column caption="Reference No" dataField="group_ticket_number" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Channel" dataField="ticket_source" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="Date Create" dataField="date_create" />
                    <Column caption="User Create" dataField="user_create" />
                    <Column caption="Department" dataField="department_name" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="Category" dataField="category_name" />
                    <Column caption="Category Product" dataField="category_sublv1_name" />
                    <Column caption="Category Case" dataField="category_sublv2_name" />
                    <Column caption="Category Detail" dataField="category_sublv3_name" />
                    <Column caption="SLA (Days)" dataField="sla" />
                    <Column caption="Dispatch Department" dataField="dispatch_to_department" />
                    <Column caption="Complaint Detail" dataField="complaint_detail" />
                    <Column caption="Response Detail" dataField="response_detail" />
                    <Column caption="Ticket Type" dataField="ticket_type" />
                    <Column caption="Last Response By" dataField="last_response_by" />
                    <Column caption="Last Response Date" dataField="last_response_at" />
                </DataGrid>
            </Container>
        </MainContent>
    )
}

export default TicketHistory

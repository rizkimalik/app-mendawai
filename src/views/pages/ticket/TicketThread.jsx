import React, { useEffect } from 'react'
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { setTicketThread } from 'app/slice/sliceTicket';
import FormGroup from 'views/components/FormGroup';
import FormInput from 'views/components/FormInput';
import RandomString from 'views/components/RandomString';
import { apiInsertCustomerEmail, apiInsertCallHistory } from 'app/services/apiSosmed';
import { authUser } from 'app/slice/sliceAuth';

function TicketThread() {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);

    const search = useLocation().search;
    const { register, formState: { errors }, reset } = useForm();
    const [subject, setSubject] = useState('');

    useEffect(() => {
        const thread_id = new URLSearchParams(search).get('thread_id');
        const thread_channel = new URLSearchParams(search).get('channel');
        const account = new URLSearchParams(search).get('account');

        reset({
            thread_id: thread_id ?? RandomString(20),
            thread_channel,
            account,
        });

        //? insert data popup pbx call
        // if (thread_channel === 'Call' && customer.customer_id) {
        if (thread_channel === 'Call') {
            dispatch(apiInsertCallHistory({
                // customer_id: customer?.customer_id,
                interaction_id: thread_id ?? RandomString(20),
                phone_number: account,
                agent_handle: username,
            }));
        }
        else if (thread_channel === 'Email') {
            dispatch(apiInsertCustomerEmail({
                interaction_id: thread_id ?? RandomString(20),
                email: account,
                agent_handle: username,
            }));
        }
    }, [username, search, reset, dispatch]);

    useEffect(() => {
        const thread_id = new URLSearchParams(search).get('thread_id');
        const thread_channel = new URLSearchParams(search).get('channel');
        const account = new URLSearchParams(search).get('account');

        dispatch(setTicketThread({
            thread_id: thread_id ?? RandomString(20),
            thread_channel,
            account,
            subject
        }));
    }, [subject, search, dispatch]);

    return (
        <div className="border rounded p-4 my-2">
            <div className="d-flex align-items-center justify-content-between">
                <h4>Channel Interaction</h4>
            </div>
            <form className="form">
                <div className="row">
                    <div className="col-lg-4 m-0">
                        <FormInput
                            name="thread_id"
                            type="text"
                            label="Interaction ID"
                            className="form-control form-control-sm"
                            placeholder="Enter InteractionID"
                            register={register}
                            rules={{ required: true, maxLength: 100 }}
                            readOnly={true}
                            errors={errors.thread_id}
                        />
                    </div>
                    <div className="col-lg-4">
                        <FormInput
                            name="thread_channel"
                            type="text"
                            label="Channel"
                            className="form-control form-control-sm"
                            placeholder="Enter Thread Channel"
                            register={register}
                            rules={{ required: true, maxLength: 100 }}
                            readOnly={false}
                            errors={errors.thread_channel}
                        />
                    </div>
                    <div className="col-lg-4">
                        <FormInput
                            name="account"
                            type="text"
                            label="Account (email, phone, userid)"
                            className="form-control form-control-sm"
                            placeholder="Enter Account"
                            register={register}
                            rules={{ required: true, maxLength: 100 }}
                            readOnly={false}
                            errors={errors.account}
                        />
                    </div>
                </div>
                <FormGroup label="Subject">
                    <textarea name="subject" onChange={(e) => setSubject(e.target.value)} value={subject} className="form-control form-control-sm" placeholder="Enter Subject" cols="10" rows="3"></textarea>
                </FormGroup>
            </form>
        </div>
    )
}

export default TicketThread
import React, { useEffect, useRef } from 'react'
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import * as echarts from 'echarts';

import { ButtonRefresh } from 'views/components/button';
import { apiDashDataAnalitycSentiment, apiDashSentimentByChannel, apiDashSentimentByWeek, apiDashTotalBySentiment } from 'app/services/apiDashboard';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';

function DashboardSentiment() {
    const dispatch = useDispatch();
    const refTotalBySentiment = useRef();
    const refSentimentByChannel = useRef();
    const refSentimentByWeek = useRef();
    const { data_analityc_sentiment, total_by_sentiment, sentiment_by_channel, sentiment_by_week } = useSelector(state => state.dashboard);

    useEffect(() => {
        dispatch(apiDashDataAnalitycSentiment({ action: 'Today' }));
        dispatch(apiDashTotalBySentiment({ action: 'Today' }));
        dispatch(apiDashSentimentByChannel({ action: 'Today' }));
        dispatch(apiDashSentimentByWeek());
    }, [dispatch]);

    useEffect(() => {
        function LoadChartTotalBySentiment() {
            const chartTotalBySentiment = echarts.init(refTotalBySentiment.current);
            const option = {
                legend: {
                    bottom: 'bottom'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                series: [{
                    name: 'Total By Sentiment',
                    type: 'pie',
                    roseType: 'area',
                    center: ['50%', '50%'],
                    itemStyle: {
                        borderRadius: 8
                    },
                    data: []
                }]
            }

            let data_series = [];
            for (let i = 0; i < total_by_sentiment?.length; i++) {
                let color_bar = '';
                if (total_by_sentiment[i].sentiment === 'positive') {
                    color_bar = '#3ba272'
                }
                else if (total_by_sentiment[i].sentiment === 'negative') {
                    color_bar = '#ee6666'
                }
                else if (total_by_sentiment[i].sentiment === 'neutral') {
                    color_bar = '#fac858'
                }
                
                data_series.push({
                    name: total_by_sentiment[i].sentiment,
                    value: total_by_sentiment[i].total,
                    itemStyle: {
                        color: color_bar
                    },
                    label: {
                        show: true,
                        position: 'inner',
                        formatter: '{d}%'
                    },
                });
            }
            option.series[0].data = data_series;
            option && chartTotalBySentiment.setOption(option);
        }
        LoadChartTotalBySentiment()

        function LoadChartSentimentByChannel() {
            const chartSentimentByChannel = echarts.init(refSentimentByChannel.current);
            const option = {
                legend: { show: false },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    top: '3%',
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                dataset: {
                    dimensions: ['channel', 'Positive', 'Negative', 'Neutral'],
                    source: []
                    // source: [  { channel: 'Matcha', Positive: 43.3, Negative: 85.8, Neutral: 93.7 }]
                },
                xAxis: {},
                yAxis: { type: 'category' },
                series: [
                    { type: 'bar', stack: 'one', showBackground: true, label: { show: true }, color: '#34E769' }, //* Positive
                    { type: 'bar', stack: 'one', showBackground: true, label: { show: true }, color: '#FF6F7D' }, //! Negative
                    { type: 'bar', stack: 'one', showBackground: true, label: { show: true }, color: '#FDD177' } //> Neutral
                ]
            };

            let data_source = [];
            for (let i = 0; i < sentiment_by_channel?.length; i++) {
                data_source.push({
                    channel: sentiment_by_channel[i].channel,
                    Positive: sentiment_by_channel[i].total_positive,
                    Negative: sentiment_by_channel[i].total_negative,
                    Neutral: sentiment_by_channel[i].total_neutral,
                });
            }
            option.dataset.source = data_source;
            option && chartSentimentByChannel.setOption(option);
        }
        LoadChartSentimentByChannel()

        function LoadChartSentimentByWeek() {
            const chartSentimentByWeek = echarts.init(refSentimentByWeek.current);
            const option = {
                legend: {
                    orient: 'vertical',
                    right: 'right'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    top: '3%',
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                dataset: {
                    dimensions: ['day_name', 'Positive', 'Negative', 'Neutral'],
                    source: []
                },
                xAxis: { type: 'category' },
                yAxis: {},
                series: [
                    { type: 'bar', barGap: 0, showBackground: true, label: { show: true, position: 'insideBottom', distance: '50', rotate: '90' }, color: '#5470c6' }, //* Positive
                    { type: 'bar', barGap: 0, showBackground: true, label: { show: true, position: 'insideBottom', distance: '50', rotate: '90' }, color: '#9a60b4' }, //! Negative
                    { type: 'bar', barGap: 0, showBackground: true, label: { show: true, position: 'insideBottom', distance: '50', rotate: '90' }, color: '#fc8452' } //> Neutral
                ]
            };

            let data_source = [];
            for (let i = 0; i < sentiment_by_week?.length; i++) {
                data_source.push({
                    day_name: sentiment_by_week[i].day_name,
                    Positive: sentiment_by_week[i].total_positive,
                    Negative: sentiment_by_week[i].total_negative,
                    Neutral: sentiment_by_week[i].total_neutral,
                });
            }
            option.dataset.source = data_source;
            option && chartSentimentByWeek.setOption(option);
        }
        LoadChartSentimentByWeek()
    }, [total_by_sentiment, sentiment_by_channel, sentiment_by_week]);


    function LoadDataToday() {
        dispatch(apiDashDataAnalitycSentiment({ action: 'Today' }));
        dispatch(apiDashTotalBySentiment({ action: 'Today' }));
        dispatch(apiDashSentimentByChannel({ action: 'Today' }));
        dispatch(apiDashSentimentByWeek());
    }

    function LoadDataWeek() {
        dispatch(apiDashDataAnalitycSentiment({ action: 'Week' }));
        dispatch(apiDashTotalBySentiment({ action: 'Week' }));
        dispatch(apiDashSentimentByChannel({ action: 'Week' }));
        // dispatch(apiDashSentimentByWeek());
    }

    function LoadDataMonth() {
        dispatch(apiDashDataAnalitycSentiment({ action: 'Month' }));
        dispatch(apiDashTotalBySentiment({ action: 'Month' }));
        dispatch(apiDashSentimentByChannel({ action: 'Month' }));
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="Dashboard" modul_name="Sentiment Analysis">
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <Link to="#tab_month" className="nav-link py-2 px-4" data-toggle="tab" onClick={() => LoadDataMonth()}>Month</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#tab_week" className="nav-link py-2 px-4" data-toggle="tab" onClick={() => LoadDataWeek()}>Week</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#tab_today" className="nav-link py-2 px-4 active" data-toggle="tab" onClick={() => LoadDataToday()}>Today</Link>
                    </li>
                </ul>
                <ButtonRefresh onClick={(e) => LoadDataToday()} />
            </SubHeader>
            <Container>
                <div className="row mb-10">
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table className="table table-head-custom table-head-bg table-borderless mb-0">
                                        <thead>
                                            <tr className="text-left text-uppercase">
                                                <th style={{ width: 400 }} className="pl-7">Text Sentences</th>
                                                <th style={{ width: 150 }}>Channel</th>
                                                <th style={{ width: 100 }}>Sentiment</th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div style={{ height: '520px', width: '100%', overflow: 'auto' }}>
                                        <table className="table table-head-custom table-head-bg table-borderless mt-0">
                                            <tbody>
                                                {
                                                    data_analityc_sentiment.map((value, index) => {
                                                        return <tr className="border-bottom" key={index}>
                                                            <td className="pl-0 py-5" style={{ width: 400 }}>
                                                                <div className="d-flex align-items-center">
                                                                    <div className="mr-4">
                                                                        <p className="text-dark-75 font-weight-bolder mb-1 font-size-lg">{value.name}</p>
                                                                        <span className="text-muted d-block">{value.message}</span>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td className="pl-0 py-5" style={{ width: 150 }}>
                                                                <div>
                                                                    <span className="d-block font-weight-bolder mb-1">{value.channel}</span>
                                                                    <span className="text-muted d-block">{value.date_create}</span>
                                                                </div>
                                                            </td>
                                                            <td className="pr-0 py-5" style={{ width: 100 }}>
                                                                {value.sentiment === 'neutral' && <span className="label label-lg label-light-warning label-inline">Neutral</span>}
                                                                {value.sentiment === 'positive' && <span className="label label-lg label-light-success label-inline">Positive</span>}
                                                                {value.sentiment === 'negative' && <span className="label label-lg label-light-danger label-inline">Negative</span>}
                                                            </td>
                                                        </tr>
                                                    })
                                                }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="card-header border-0 p-4">
                                        <h3 className="font-weight-bolder text-dark m-0 p-0">Overall Sentiment</h3>
                                    </div>
                                    <div className="card-body p-4">
                                        <div className="mb-5 p-0" ref={refTotalBySentiment} style={{ height: '220px', width: '100%' }}></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="card-header border-0 p-4">
                                        <h3 className="font-weight-bolder text-dark m-0 p-0">Sentiment By Channel</h3>
                                    </div>
                                    <div className="card-body p-4">
                                        <div className="mb-5 p-0" ref={refSentimentByChannel} style={{ height: '220px', width: '100%' }}></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-lg-12">
                                <div className="card">
                                    <div className="card-header border-0 p-4">
                                        <h3 className="font-weight-bolder text-dark m-0 p-0">Sentiment This Week</h3>
                                    </div>
                                    <div className="card-body p-4">
                                        <div className="mb-5 p-0" ref={refSentimentByWeek} style={{ height: '220px', width: '100%' }}></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default DashboardSentiment
import React, { useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import * as echarts from 'echarts';

import { ButtonRefresh } from 'views/components/button';
import Icons from 'views/components/Icons';
import { Card, CardBody, CardHeader, CardTitle } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { authUser } from 'app/slice/sliceAuth';
import { apiDashChartChannel, apiDashChartTotal, apiDashDataTicket, apiDashTotalTicket } from 'app/services/apiDashboard';
import BucketStatusTicket from './BucketStatusTicket';

function DashboardTicket() {
    const refTotalTicket = useRef();
    const refChannelTicket = useRef();
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const { total_ticket, data_ticket, chart_by_channel, chart_by_total } = useSelector(state => state.dashboard);

    useEffect(() => {
        const data = {
            action: 'Today',
            user_create: user.username,
            department_id: user.department,
            user_level: user.user_level,
        }
        dispatch(apiDashTotalTicket(data));
        dispatch(apiDashDataTicket(data));
        dispatch(apiDashChartChannel({ action: 'Today' }));
        dispatch(apiDashChartTotal({ action: 'Today' }));
    }, [dispatch, user]);

    useEffect(() => {
        function LoadChartWeek() {
            const colors = ['#023163', '#003D89', '#0078B5', '#0295C8', '#01B5D8', '#48CAE2', '#91E0EF'];
            const chartTicketPerWeek = echarts.init(refTotalTicket.current);
            const option = {
                title: {
                    text: 'Overall Total Ticket'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                xAxis: {
                    type: 'category',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                series: [{
                    type: 'bar',
                    data: []
                }]
            }

            let xAxis_data = [];
            let yAxis_data = [];
            for (let i = 0; i < chart_by_total.length; i++) {
                xAxis_data.push(chart_by_total[i].label_name);
                yAxis_data.push({
                    value: chart_by_total[i].total_ticket,
                    itemStyle: {
                        color: colors[i]
                    },
                    label: { show: true, position: 'insideBottom', distance: '50', rotate: '90', formatter: '{b} : {c}' },
                });
            }
            option.xAxis.data = xAxis_data;
            option.series[0].data = yAxis_data;

            option && chartTicketPerWeek.setOption(option);
        }
        LoadChartWeek();

        function LoadChartChannel() {
            const chartChannelTicket = echarts.init(refChannelTicket.current);
            let yAxis_data = [];
            const option = {
                title: {
                    text: 'Ticket by Channel'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    right: 'right',
                    top: 'top'
                },
                series: [{
                    name: 'Ticket by Channel',
                    data: [],
                    type: 'pie',
                    radius: ['40%', '70%'],
                    avoidLabelOverlap: false,
                    itemStyle: {
                        borderRadius: 10,
                        borderColor: '#fff',
                        borderWidth: 2
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: 30,
                            fontWeight: 'bold'
                        }
                    },
                }]
            };

            for (let i = 0; i < chart_by_channel.length; i++) {
                yAxis_data.push({
                    name: chart_by_channel[i].channel,
                    value: chart_by_channel[i].total,
                    label: { show: true, position: 'inner', formatter: '{b} : {c} ({d}%)' },
                });
            }
            option.series[0].data = yAxis_data;
            option && chartChannelTicket.setOption(option);
        }
        LoadChartChannel();
    });

    function LoadDataToday() {
        const data = {
            action: 'Today',
            user_create: user.username,
            department_id: user.department,
            user_level: user.user_level
        }
        dispatch(apiDashTotalTicket(data));
        dispatch(apiDashDataTicket(data));
        dispatch(apiDashChartTotal({ action: 'Today' }))
        dispatch(apiDashChartChannel({ action: 'Today' }))
    }

    function LoadDataLast7Days() {
        const data = {
            action: 'Last7Days',
            user_create: user.username,
            department_id: user.department,
            user_level: user.user_level,
        }
        dispatch(apiDashTotalTicket(data));
        dispatch(apiDashDataTicket(data));
        dispatch(apiDashChartTotal({ action: 'Last7Days' }))
        dispatch(apiDashChartChannel({ action: 'Last7Days' }))
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="Dashboard" modul_name="Ticket">
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <Link to="#tab_7days" className="nav-link py-2 px-4" data-toggle="tab" onClick={() => LoadDataLast7Days()}>7 Days</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#tab_today" className="nav-link py-2 px-4 active" data-toggle="tab" onClick={() => LoadDataToday()}>Today</Link>
                    </li>
                </ul>
                <ButtonRefresh onClick={(e) => LoadDataToday()} />
            </SubHeader>
            <Container>
                <h3 className="font-weight-bolder text-dark mb-4">Total By Status</h3>
                <BucketStatusTicket total_ticket={total_ticket} />
                <div className="row mb-10">
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-body" style={{ height: '400px', width: '100%' }} ref={refTotalTicket} />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-body" style={{ height: '400px', width: '100%' }} ref={refChannelTicket} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardHeader className="border-0">
                                <CardTitle title={`Status SLA Percentage`} subtitle="Data Ticket SLA ." />
                            </CardHeader>
                            <CardBody>
                                <DataGrid
                                    dataSource={data_ticket}
                                    remoteOperations={{
                                        filtering: true,
                                        sorting: true,
                                        paging: true
                                    }}
                                    allowColumnReordering={true}
                                    allowColumnResizing={true}
                                    columnAutoWidth={true}
                                    showBorders={true}
                                    showColumnLines={true}
                                    showRowLines={true}
                                >
                                    <HeaderFilter visible={true} />
                                    <FilterRow visible={true} />
                                    <Paging defaultPageSize={10} />
                                    <Pager
                                        visible={true}
                                        displayMode='full'
                                        allowedPageSizes={[10, 20, 50]}
                                        showPageSizeSelector={true}
                                        showInfo={true}
                                        showNavigationButtons={true} />
                                    <Column caption="Ticket Number" dataField="ticket_number" cellRender={(data) => {
                                        return <button type="button" className="btn btn-sm btn-light-primary py-1 px-2">
                                            <Icons iconName="ticket" className="svg-icon svg-icon-sm p-0" />
                                            {data.value}
                                        </button>
                                    }} />
                                    <Column caption="Ticket on Layer" dataField="dispatch_to_layer" />
                                    <Column caption="SLA (Days)" dataField="sla" />
                                    <Column caption="Total SLA (Days)" dataField="total_sla" />
                                    <Column caption="Percentage SLA %" dataField="sla_percentage" cellRender={(data) => {
                                        return data.value < 100 ? <span className="text-danger">{data.value} % </span> : <span className="text-success">{data.value} % </span>;
                                    }} />
                                    <Column caption="Status" dataField="status" />
                                    <Column caption="Date Created" dataField="date_create" />
                                    <Column caption="Date Closed" dataField="date_closed" />
                                </DataGrid>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default DashboardTicket

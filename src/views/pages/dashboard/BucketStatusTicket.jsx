import React from 'react'
const bg_card = ['bg-primary', 'bg-warning', 'bg-success', 'bg-danger', 'bg-info', 'bg-light', 'bg-dark'];

function BucketStatusTicket({ total_ticket }) {
    return (
        <div className="row">
            {
                total_ticket.map((item, index) => {
                    return (
                        <div className="col-md-3" key={index}>
                            <div className={`card card-custom ${bg_card[index]} card-stretch gutter-b`}>
                                <div className="card-body">
                                    <i className={`icon-2x text-white ${item.icon}`} />
                                    <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{item.total}</span>
                                    <span className="font-weight-bold text-white font-size-sm">Ticket {item.status}</span>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default BucketStatusTicket
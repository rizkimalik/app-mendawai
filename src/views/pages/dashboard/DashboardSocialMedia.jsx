import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { ButtonRefresh } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import IconBrand from 'views/components/IconBrand';
import { apiDashSocmed_AgentOnline, apiDashSocmed_ChannelInteraction, apiDashSocmed_TotalChannel } from 'app/services/apiDashboard';

function DashboardSocialMedia() {
    const dispatch = useDispatch();
    const { agent_online, total_channel, channel_interaction } = useSelector(state => state.dashboard);

    useEffect(() => {
        dispatch(apiDashSocmed_AgentOnline())
        dispatch(apiDashSocmed_TotalChannel())
        dispatch(apiDashSocmed_ChannelInteraction())

        // const intervalId = setInterval(() => {
        //     dispatch(apiDashSocmed_AgentOnline())
        //     dispatch(apiDashSocmed_TotalChannel())
        //     dispatch(apiDashSocmed_ChannelInteraction())
        // }, 15 * 1000)
        // return () => clearInterval(intervalId);
    }, [dispatch])

    function RefreshData() {
        dispatch(apiDashSocmed_AgentOnline())
        dispatch(apiDashSocmed_TotalChannel())
        dispatch(apiDashSocmed_ChannelInteraction())
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="Dashboard" modul_name="Channel">
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <a className="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_11_3">Today</a>
                    </li>
                </ul>
                <ButtonRefresh onClick={(e) => RefreshData()} />
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-md-3">
                        <div className={`card card-custom card-stretch gutter-b border`}>
                            <div className="card-body d-flex align-items-center justify-content-between p-6">
                                <div className="symbol symbol-45 symbol-circle border-1">
                                    <div className="symbol-label">All</div>
                                </div>
                                <div className="d-flex flex-column align-items-end">
                                    <span className="font-weight-bolder font-size-h2 mb-0 d-block">
                                        {total_channel.map((item) => item.total).reduce((total, a) => total + a, 0)}
                                    </span>
                                    <span className="font-weight-bold font-size-sm d-block">Incoming All</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    {
                        total_channel.map((item, index) => {
                            return (
                                <div className="col-md-3" key={index}>
                                    <div className={`card card-custom card-stretch gutter-b border`}>
                                        <div className="card-body d-flex align-items-center justify-content-between p-6">
                                            <div className="symbol symbol-45 symbol-circle border-1">
                                                <div className="symbol-label">
                                                    <IconBrand name={item.channel} height={45} width={45} />
                                                </div>
                                            </div>
                                            <div className="d-flex flex-column align-items-end">
                                                <span className="font-weight-bolder font-size-h2 mb-0 d-block">{item.total}</span>
                                                <span className="font-weight-bold font-size-sm d-block">Incoming {item.channel}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>

                <div className="row mb-10">
                    <div className="col-lg-8">
                        <div className="card p-0">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Last Data Interaction.</div>
                                </div>
                            </div>
                            <div className="card-body p-2">
                                <DataGrid
                                    dataSource={channel_interaction}
                                    remoteOperations={{
                                        filtering: true,
                                        sorting: true,
                                        paging: true
                                    }}
                                    allowColumnReordering={true}
                                    allowColumnResizing={true}
                                    columnAutoWidth={true}
                                    showBorders={true}
                                    showColumnLines={true}
                                    showRowLines={true}
                                >
                                    <HeaderFilter visible={true} />
                                    <FilterRow visible={true} />
                                    <Paging defaultPageSize={5} />
                                    <Pager
                                        visible={true}
                                        allowedPageSizes={[5, 20, 50]}
                                        displayMode='full'
                                        showPageSizeSelector={true}
                                        showInfo={true}
                                        showNavigationButtons={true} />
                                    <GroupPanel visible={true} />
                                    <Column caption="Channel" dataField="channel" />
                                    <Column caption="Interaction ID" dataField="interaction_id" />
                                    <Column caption="Account" dataField="user_id" />
                                    <Column caption="Agent Handle" dataField="agent_handle" />
                                    <Column caption="Message" dataField="message" />
                                    <Column caption="Customer ID" dataField="customer_id" />
                                    <Column caption="Customer Name" dataField="customer_name" />
                                    <Column caption="Datetime" dataField="datetime" />
                                </DataGrid>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4">
                        <div className="card">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Agent State</div>
                                    <span className="label label-light-primary label-inline mx-2">{agent_online?.length}</span>
                                </div>
                            </div>
                            <div className="card-body p-2" style={{ height: '400px', width: '100%' }}>
                                {
                                    agent_online.map((item, index) => {
                                        return (
                                            <div className="list list-hover mb-4" key={index}>
                                                <div className='list-item d-flex align-items-center justify-content-between'>
                                                    <div className="d-flex align-items-center py-2 mx-2">
                                                        <span className="bullet bullet-bar bg-success align-self-stretch mr-2"></span>
                                                        <div className="symbol symbol-40px">
                                                            <div className="symbol-label">
                                                                <i className="fa fa-user text-success"></i>
                                                            </div>
                                                        </div>
                                                        <div className="flex-grow-1 mx-2">
                                                            <div className="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg mx-2">Max Handle - {item.username}</div>
                                                            <div className="mt-2">
                                                                <span className="text-warning m-2"><i className="far fa-comment text-warning"></i> {item.max_chat}</span>
                                                                <span className="text-info m-2"><i className="far fa-envelope text-info"></i> {item.max_email}</span>
                                                                <span className="text-success m-2"><i className="fab fa-whatsapp text-success"></i> {item.max_whatsapp}</span>
                                                                <span className="text-primary m-2"><i className="fab fa-facebook text-primary"></i> {item.max_facebook}</span>
                                                                <span className="text-danger m-2"><i className="fab fa-instagram text-danger"></i> {item.max_instagram}</span>
                                                                <span className="text-primary m-2"><i className="fab fa-twitter text-primary"></i> {item.max_twitter}</span>
                                                                <span className="text-mute m-2"><i className="fas fa-phone-square text-dark"></i> {item.max_inbound}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-column align-items-end mx-2">
                                                        <div className="text-muted">Handle</div>
                                                        <div className="d-flex align-items-center justify-content-between mt-2">
                                                            <span className="label label-light-primary font-weight-bold label-inline mx-2">{item.total_handle}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>

                {/* <div className="row mb-20">
                    <div className="col-lg-12">
                        <div className="card p-0">
                            <div className="card-header">
                                <div className='d-flex align-items-center justify-content-between'>
                                    <div>Activity Detail</div>
                                    <span className="label label-light-primary label-inline mx-2">156</span>
                                </div>
                            </div>
                            <div className="card-body" style={{ height: '350px', width: '100%' }} ref={chartDom} />
                        </div> 
                    </div>
                </div> */}
            </Container >
        </MainContent >
    )
}

export default DashboardSocialMedia

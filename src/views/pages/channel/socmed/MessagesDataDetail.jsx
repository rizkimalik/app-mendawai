import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { apiInteractionDetail } from 'app/services/apiSosmed';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import Attachment from './Attachment';

function MessagesDataDetail({ interaction }) {
    const dispatch = useDispatch();
    const { interaction_detail } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(apiInteractionDetail({
            interaction_id: interaction.chat_id,
            customer_id: interaction.customer_id,
            channel: interaction.channel,
            channel_type: ''
        }));
    }, [dispatch, interaction])

    return (
        <Modal id="modalMessagesDataDetail" modal_size="modal-lg">
            <ModalHeader title='Conversation Detail.' />
            <ModalBody className="p-0">
                <div className="row">
                    <div className="col-lg-12">
                        <div style={{ height: '400px', overflow: 'auto' }}>
                            <div className="messages p-8">
                                {
                                    interaction_detail?.map((item, index) => {
                                        if (item.flag_to === 'customer') {
                                            return (
                                                <div className="d-flex justify-content-start mb-10" key={index}>
                                                    <div className="d-flex flex-column align-items-start">
                                                        <div className="d-flex align-items-center mb-2">
                                                            <div className="ml-3">
                                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary">{item.name}</span>
                                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                                            </div>
                                                        </div>
                                                        <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                                            {item.attachment && <Attachment value={item.attachment} />}
                                                            {item.message}
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        }
                                        else if (item.flag_to === 'agent') {
                                            return (
                                                <div className="d-flex justify-content-end mb-10" key={index}>
                                                    <div className="d-flex flex-column align-items-end">
                                                        <div className="d-flex align-items-center mb-2">
                                                            <div className="mr-3">
                                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">{item.name}</span>
                                                            </div>
                                                        </div>
                                                        <div className="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">
                                                            {item.attachment && <Attachment value={item.attachment} />}
                                                            {item.message}
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        }

                                        return ('');
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </ModalBody>
            <ModalFooter></ModalFooter>
        </Modal>
    )
}

export default MessagesDataDetail
import React, { useRef } from 'react'

function FormSendMessage({ message, setMessage, sendMessage, onKeyTyping, attachment, setAttachment }) {
    const inputFileRef = useRef(null);

    const onFileChange = (e) => {
        console.log(e.target.files[0]);
        if (e.target.files[0].size > 3000000) {
            alert('Max size file 3mb.');
        } else {
            setAttachment(e.target.files[0]);
        }
        e.target.value = "";
    }

    const onFileClose = (e) => {
        setAttachment('')
        e.target.value = "";
    }

    return (
        <div className="card-footer bg-light p-0">
            {
                attachment &&
                <div className="alert alert-custom alert-notice alert-light-primary fade show p-2" role="alert">
                    <div className="alert-icon"><i className="fa fa-file-image" /></div>
                    <div className="alert-text">{attachment?.name}</div>
                    <div className="alert-close">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={onFileClose}>
                            <span aria-hidden="true"><i className="ki ki-close" /></span>
                        </button>
                    </div>
                </div>
            }
            <div className="form-group m-4">
                <div className="input-group">
                    <div className="input-group-prepend">
                        <input type="file" ref={inputFileRef} onChange={onFileChange} accept="image/*, .doc, .pdf, .xls, .xlsx" className="hide" />
                        <button className="btn btn-secondary" type="button" onClick={() => inputFileRef.current.click()}><i className="fa fa-paperclip" /></button>
                    </div>
                    <input style={{zIndex: 1000}}
                        type="text"
                        name="message"
                        className="form-control"
                        placeholder="Type a message"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        onKeyPress={e => e.key === 'Enter' ? sendMessage(e) : onKeyTyping()}
                    />
                    <div className="input-group-append">
                        <button onClick={sendMessage} className="btn btn-primary" type="button">Send</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FormSendMessage
import React from 'react'
import { useSelector } from 'react-redux';
import IconBrand from 'views/components/IconBrand';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { authUser } from 'app/slice/sliceAuth';

function ListCustomer({ handlerSelectCustomer }) {
    const { username } = useSelector(authUser);
    const { list_customers, status, selected_customer } = useSelector(state => state.sosialmedia);
    const get_customers = list_customers.data;

    return (
        <div className="flex-row-auto offcanvas-mobile w-350px w-xl-400px offcanvas-mobile-on">
            <Card>
                <CardHeader>
                    <CardTitle title={username} subtitle={status.socket_id !== null ? <span className="text-success">Available</span> : 'Not Ready'} />
                    <CardToolbar>
                        <span className="font-weight-bold font-size-sm mr-2">Live</span>
                        <span className="label label-rounded label-light-primary">{get_customers ? get_customers.length : 0}</span>
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-0">
                    <div style={{ height: 'calc(75vh - 60px)', overflow: 'auto' }}>
                        {
                            get_customers?.map((customer, index) => {
                                return (
                                    <div className="list list-hover border-bottom" key={index} onClick={() => handlerSelectCustomer(customer)}>
                                        <div className={`list-item d-flex align-items-center justify-content-between ${customer.chat_id === selected_customer?.chat_id ? 'active' : ''}`}>
                                            <div className="d-flex align-items-center w-50 py-4 mx-2">
                                                <div className="symbol symbol-45px symbol-circle">
                                                    <div className="symbol-label fw-bolder">
                                                        <IconBrand name={(customer.channel).split('_')[0]} height={45} width={45} />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 mx-2">
                                                    <div className="mr-2">{customer.name}</div>
                                                    <div className="text-muted mt-2">{customer.user_id}</div>
                                                </div>
                                            </div>
                                            <div className="d-flex flex-column align-items-end mx-2">
                                                {/* <span className="label label-sm label-rounded label-success">{customer.total_chat}</span> */}
                                                <div className="d-flex align-items-center justify-content-between mt-2">
                                                    <span className="label label-light-primary font-weight-bold label-inline mx-1">{customer.channel}</span>
                                                    <span className="label label-sm label-rounded label-success mx-1">{customer.total_chat}</span>
                                                </div>
                                                <span className="text-muted mt-2">{customer.page_name}</span>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </CardBody>
            </Card>
        </div>
    )
}

export default ListCustomer
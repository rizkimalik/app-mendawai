import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { Link, NavLink, useHistory } from 'react-router-dom';

import { socket, urlAttachment } from 'app/config';
import { Datetime } from 'views/components/Datetime';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { authUser } from 'app/slice/sliceAuth';
import { getListCustomer, getLoadConversation, getEndChat, setAccountSpam } from 'app/services/apiSosmed';
import { setSelectedCustomer } from 'app/slice/sliceSosmed';
import { ListCustomer, Conversation, FormSendMessage } from '.';
import CustomerJourney from 'views/pages/customer/CustomerJourney';
import { setTicketThread } from 'app/slice/sliceTicket';
import { IconClock, IconSetting, IconUserGroup } from 'views/components/icon';

const SocialMedia = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const refConversation = useRef();
    const refContentChat = useRef();
    const { username } = useSelector(authUser);
    const { selected_customer, conversations } = useSelector(state => state.sosialmedia);
    const [navigate, setNavigate] = useState('');
    const [message, setMessage] = useState('');
    const [conversation, setConversation] = useState([]);
    const [attachment, setAttachment] = useState('');
    const [typing, setTyping] = useState(false);
    const [load_customer, setLoadCustomer] = useState(false);

    useEffect(() => {
        dispatch(getListCustomer({ agent_handle: username }))
    }, [dispatch, username, load_customer]);

    useEffect(() => {
        function showMessageContent(res) {
            setLoadCustomer(true)
            if (selected_customer) {
                if (res.chat_id === selected_customer.chat_id) {
                    setConversation(conversation => [...conversation, res]);
                    refContentChat.current.scrollTo(0, refConversation.current.scrollHeight); //? auto scroll to down
                }
            }
        }

        setTyping(false);
        // socket.on('return-typing', (res) => {
        //     setTyping(res.typing);
        // });
        socket.on('send-message-customer', (res) => { //push from blending
            showMessageContent(res);
        });
        socket.on('return-message-customer', (res) => {
            showMessageContent(res);
        });
        socket.on('return-message-whatsapp', (res) => {
            showMessageContent(res);
        });
        socket.on('return-directmessage-twitter', (res) => {
            showMessageContent(res);
        });
        setLoadCustomer(false)
    }, [selected_customer]);

    useEffect(() => {
        setConversation(conversations.data);
    }, [conversations]);


    function sendMessage(e) {
        e.preventDefault();

        let message_type = 'text';
        let attachment_file = '';
        if (attachment) {
            let generate_filename = Date.now() + '.' + (attachment.name).split('.').pop();
            message_type = (attachment.type).split('/')[0] === 'image' ? 'image' : 'document';
            attachment_file = [{
                attachment: attachment,
                file_origin: attachment?.name,
                file_name: generate_filename,
                file_size: attachment?.size,
                file_url: attachment ? `${urlAttachment}/${selected_customer.channel}/${generate_filename}` : '',
            }];
        }

        let values = {
            username: selected_customer.agent_handle,
            chat_id: selected_customer.chat_id,
            user_id: selected_customer.user_id,
            customer_id: selected_customer.customer_id,
            message: message,
            message_type: message_type,
            name: username,
            email: selected_customer.email,
            channel: selected_customer.channel,
            flag_to: 'agent',
            agent_handle: username,
            page_id: selected_customer.page_id,
            date_create: Datetime(),
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            attachment: attachment_file,
            typing: false
        }

        if (message) {
            if (values.channel === 'Chat') {
                socket.emit('send-message-agent', values);
            }
            else if (values.channel === 'Whatsapp') {
                socket.emit('send-message-whatsapp', values);
            }
            else if (values.channel === 'Twitter_DM') {
                socket.emit('send-directmessage-twitter', values);
            }
            setConversation(conversation => [...conversation, values]);
            setTimeout(() => {
                refContentChat.current.scrollTo(0, refConversation.current.scrollHeight); //? auto scroll to down
            }, 500);
        }
        setMessage('');
        setAttachment('');
    }

    function onKeyTyping() {
        let data = {
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            email: selected_customer.email,
            agent_handle: selected_customer.agent_handle,
            flag_to: 'agent',
            typing: true
        }
        socket.emit('typing', data);
    }

    function handlerSelectCustomer(customer) {
        dispatch(setSelectedCustomer(customer))
        dispatch(getListCustomer({ agent_handle: username }))
        dispatch(getLoadConversation({
            chat_id: customer.chat_id,
            customer_id: customer.customer_id
        }))

        setTimeout(() => {
            refContentChat.current.scrollTo(0, refConversation.current.scrollHeight); //? auto scroll to down
        }, 500);
    }

    function handlerEndChat(value) {
        const { chat_id, customer_id, user_id, channel } = value;
        Swal.fire({
            title: 'End Session',
            text: 'Do you want to close the conversations?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'End Session',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Add to Ticket',
                    text: 'open form create ticket now.',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Create Ticket',
                }).then((res) => {
                    if (res.isConfirmed) {
                        dispatch(getEndChat({ chat_id, customer_id }));
                        dispatch(setSelectedCustomer({}));
                        setConversation([]);
                        history.push(`/ticket?thread_id=${chat_id}&channel=${channel}&account=${user_id}`);
                    }
                    else {
                        dispatch(setTicketThread({
                            thread_id: chat_id,
                            thread_channel: channel,
                            account: user_id,
                            subject: 'interaction data sosmed.'
                        }));
                        dispatch(getEndChat({ chat_id, customer_id }));
                        dispatch(setSelectedCustomer({}));
                        setConversation([]);
                        dispatch(getListCustomer({ agent_handle: username }));
                    }
                })
            }
        })
    }

    function handlerAccountSpam(value) {
        Swal.fire({
            title: 'Spam Chat.',
            text: 'Do you want to report the chat?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Report',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(setAccountSpam(value));
                dispatch(getListCustomer({ agent_handle: username }))
                dispatch(setSelectedCustomer({}))
                dispatch(getLoadConversation({ chat_id: value.chat_id, customer_id: value.customer_id }))
            }
        })

    }

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Live Messages">
                <NavLink to="/customer" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconUserGroup className="svg-icon svg-icon-sm" /> Data Customers
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconClock className="svg-icon svg-icon-sm" /> History Data Messages
                </NavLink>
            </SubHeader>
            <Container>
                <div className="d-flex flex-row">
                    <ListCustomer handlerSelectCustomer={handlerSelectCustomer} />
                    <div className="flex-row-fluid ml-lg-4">
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle
                                    title={selected_customer?.name}
                                    subtitle={selected_customer?.user_id && typing ? 'Typing..' : selected_customer?.user_id}
                                />
                                {
                                    selected_customer?.chat_id &&
                                    <CardToolbar>
                                        <button type="button" onClick={(e) => setNavigate('Journey')} className="btn btn-icon btn-sm btn-light-info btn-circle ml-3" title="Customer Journey" data-toggle="modal" data-target="#modalJourneyCustomer">
                                            <i className="fas fa-route fa-sm"></i>
                                        </button>
                                        <button type="button" className="btn btn-icon btn-sm btn-light-danger btn-circle ml-2" title="End Chat" onClick={(e) => handlerEndChat(selected_customer)}>
                                            <i className="far fa-window-close fa-sm"></i>
                                        </button>
                                        <button className="btn btn-sm btn-light-primary btn-icon btn-circle ml-3" data-toggle="dropdown" aria-expanded="false">
                                            <IconSetting className="svg-icon svg-icon-md" />
                                        </button>
                                        <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <ul className="navi flex-column navi-hover py-2">
                                                <li className="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Choose an action: </li>
                                                <li className="navi-item">
                                                    <NavLink to={`/customer/${selected_customer.customer_id}/edit`} className="navi-link">
                                                        <IconUserGroup className="navi-icon" /> Detail Customer
                                                    </NavLink>
                                                </li>
                                                <li className="navi-item">
                                                    <div className="dropdown-divider"></div>
                                                </li>
                                                <li className="navi-item">
                                                    <Link to="#" onClick={(e) => handlerAccountSpam(selected_customer)} className="navi-link">
                                                        <i className="far fa-eye-slash fa-lg mr-2"></i> Spam Chat
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </CardToolbar>
                                }
                            </CardHeader>
                            <CardBody className="p-0">
                                <Conversation
                                    conversation={conversation}
                                    selected_customer={selected_customer}
                                    refConversation={refConversation}
                                    refContentChat={refContentChat}
                                />
                            </CardBody>

                            {
                                selected_customer.customer_id &&
                                <FormSendMessage
                                    message={message}
                                    setMessage={setMessage}
                                    attachment={attachment}
                                    setAttachment={setAttachment}
                                    sendMessage={sendMessage}
                                    onKeyTyping={onKeyTyping}
                                />
                            }
                        </Card>
                    </div>
                </div>
                {
                    navigate === 'Journey' &&
                    selected_customer.customer_id &&
                    <CustomerJourney customer={selected_customer} />
                }
            </Container>
        </MainContent>
    )
}

export default SocialMedia
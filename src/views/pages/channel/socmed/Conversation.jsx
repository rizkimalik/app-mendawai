import React from 'react'
import Attachment from './Attachment';

function Conversation({ conversation, selected_customer, refConversation, refContentChat }) {
    return (
        <div ref={refContentChat} style={{ height: 'calc(75vh - 130px)', overflowY: 'auto' }}>
            <div ref={refConversation} className="messages p-8">
                {
                    // conversation?.map((item, index) => {
                    //? remove duplicate array 
                    [...new Set(conversation)]?.filter((item) => item.chat_id === selected_customer.chat_id).map((item, index) => {
                        if (item.flag_to === 'customer') {
                            return (
                                <div className="d-flex justify-content-start mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-start">
                                        <div className="d-flex align-items-center mb-2">
                                            <div className="ml-3">
                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary">{item.name}</span>
                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                            </div>
                                        </div>
                                        <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        else if (item.flag_to === 'agent') {
                            return (
                                <div className="d-flex justify-content-end mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-end">
                                        <div className="d-flex align-items-center mb-2">
                                            <div className="mr-3">
                                                <span className="text-muted mb-1 mx-2">{item.date_create}</span>
                                                <span className="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">{item.name}</span>
                                            </div>
                                        </div>
                                        <div className="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                        return ('');
                    })
                }
            </div>
        </div>
    )
}

export default Conversation
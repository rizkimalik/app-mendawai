import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { getDataMessages } from 'app/services/apiSosmed';
import { IconGroupChat, IconTicket } from 'views/components/icon';
import { ButtonRefresh } from 'views/components/button';
import MessagesDataDetail from './MessagesDataDetail';

function MessagesDataHistory() {
    const dispatch = useDispatch();
    const [interaction, setInteraction] = useState('');
    const { data_messages } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(getDataMessages())
    }, [dispatch])

    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                {
                    data.ticket_number === null &&
                    <NavLink to={`/ticket?thread_id=${data.chat_id}&channel=${data.channel}&account=${data.user_id}`} className="btn btn-icon btn-light-primary btn-hover-primary btn-xs mx-1" data-toggle="tooltip" title="Create Ticket">
                        <IconTicket className="svg-icon svg-icon-xs" />
                    </NavLink>
                }
                <button type="button" onClick={() => setInteraction(data)} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Open Detail" data-toggle="modal" data-target="#modalMessagesDataDetail">
                    <IconGroupChat className="svg-icon svg-icon-xs" />
                </button>
            </div>
        )
    }

    return (
        <Card>
            <CardHeader className="border-0">
                <CardTitle title="All Messages" subtitle="Data messages." />
                <CardToolbar>
                    <ButtonRefresh onClick={() => dispatch(getDataMessages())} />
                </CardToolbar>
            </CardHeader>
            <CardBody>
                <DataGrid
                    dataSource={data_messages}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        displayMode='full'
                        allowedPageSizes={[10, 20, 50]}
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Actions" dataField="id" fixed={true} cellRender={componentButtonActions} />
                    <Column caption="Interaction ID" dataField="chat_id" fixed={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="Channel" dataField="channel" />
                    <Column caption="User ID" dataField="user_id" />
                    <Column caption="Start Time" dataField="start_time" />
                    <Column caption="End Time" dataField="end_time" />
                    <Column caption="Total Time" dataField="total_time" />
                    <Column caption="Agent" dataField="agent_handle" />
                    <Column caption="Customer ID" dataField="customer_id" />
                    <Column caption="Customer Name" dataField="name" />
                </DataGrid>

                {interaction && <MessagesDataDetail interaction={interaction} />}
            </CardBody>
        </Card>
    )
}

export default MessagesDataHistory
import React from 'react'
import HtmlEditor, { Toolbar, Item, MediaResizing } from 'devextreme-react/html-editor';
import { useForm } from 'react-hook-form';

function EmailCompose() {
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmitSendEmail = async (data) => {
        data.email_body = document.getElementById('email_body').value;
        try {
            console.log(data);
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <div className="p-4 my-2">
            <div className="d-flex justify-content-between mb-5">
                <h4>Compose Email </h4>
            </div>
            <form onSubmit={handleSubmit(onSubmitSendEmail)} id="compose_form">
                <div className="d-block mb-4">
                    <div className="d-flex align-items-center border-bottom">
                        <input {...register("to", { required: true })} className="form-control border-0 px-8 min-h-45px" name="to" placeholder="To:" />
                        {errors.to && <span className="form-text text-danger">Please enter email to.</span>}
                    </div>
                    <div className="d-flex align-items-center border-bottom">
                        <input {...register("cc", { required: false })} className="form-control border-0 px-8 min-h-45px" name="cc" placeholder="CC:" />
                    </div>
                    <div className="border-bottom">
                        <input {...register("subject", { required: true })} className="form-control border-0 px-8 min-h-45px" name="subject" placeholder="Subject" />
                        {errors.subject && <span className="form-text text-danger">Please enter subject</span>}
                    </div>

                    <input type="hidden" className="form-control" id="email_body" title="tampung data body email" />
                    <HtmlEditor
                        className="my-4"
                        height={380}
                        valueType="html"
                        onValueChange={(value) => document.getElementById('email_body').value = value}
                    // defaultValue={email_body}
                    // onValueChange={(e) => htmlValue(e)}
                    >
                        <MediaResizing enabled={true} />
                        <Toolbar multiline={true}>
                            <Item name="undo" />
                            <Item name="redo" />
                            <Item name="separator" />
                            <Item
                                name="size"
                                acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                            />
                            <Item
                                name="font"
                                acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                            />
                            <Item name="separator" />
                            <Item name="bold" />
                            <Item name="italic" />
                            <Item name="strike" />
                            <Item name="underline" />
                            <Item name="separator" />
                            <Item name="alignLeft" />
                            <Item name="alignCenter" />
                            <Item name="alignRight" />
                            <Item name="alignJustify" />
                            <Item name="separator" />
                            <Item name="orderedList" />
                            <Item name="bulletList" />
                            <Item name="separator" />
                            <Item name="color" />
                            <Item name="background" />
                            <Item name="separator" />
                            <Item name="link" />
                            <Item name="image" />
                            <Item name="separator" />
                            <Item name="separator" />
                            <Item name="clear" />
                            <Item name="codeBlock" />
                            <Item name="blockquote" />
                            <Item name="separator" />
                            <Item name="insertTable" />
                            <Item name="deleteTable" />
                            <Item name="insertRowAbove" />
                            <Item name="insertRowBelow" />
                            <Item name="deleteRow" />
                            <Item name="insertColumnLeft" />
                            <Item name="insertColumnRight" />
                            <Item name="deleteColumn" />
                        </Toolbar>
                    </HtmlEditor>
                </div>
                <div className="d-flex align-items-center justify-content-between border-top py-4">
                    <div className="d-flex align-items-center">
                        <button type="submit" className="btn btn-primary font-weight-bold px-6">Send</button>
                        <button type="button" className="btn btn-icon btn-sm btn-clean">
                            <i className="flaticon2-clip-symbol" />
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default EmailCompose
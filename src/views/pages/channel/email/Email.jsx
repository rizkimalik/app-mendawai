import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { IconMailHistory, IconMailInbox, IconMailSent, IconMailSpam, IconCall, IconGroupChat } from 'views/components/icon'
import { EmailInbox, EmailSent, EmailCompose } from '.'

function Email() {
    const [navigate, setNavigate] = useState('Inbox');

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Mailbox" modul_name={navigate}>
                <NavLink to="/omnichannel/call" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconCall className="svg-icon svg-icon-sm" /> Data Call
                </NavLink>
                <NavLink to="/omnichannel/email" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconMailHistory className="svg-icon svg-icon-sm" /> Data Mailbox
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconGroupChat className="svg-icon svg-icon-sm" /> Data Sosial Media
                </NavLink>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <Card>
                            <CardBody className="px-5">
                                <div className="px-4 mt-4 mb-10">
                                    <a data-toggle="tab" href="#tabEmailCompose" onClick={(e) => setNavigate('Compose')} className="btn btn-block btn-primary btn-sm font-weight-bold text-uppercase text-center">Compose</a>
                                </div>

                                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailInbox" onClick={(e) => setNavigate('Inbox')} className={`navi-link ${navigate === 'Inbox' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailInbox className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Inbox</span>
                                            {/* <span className="navi-label">
                                                <span className="label label-rounded label-primary font-weight-bolder">{total_inbox}</span>
                                            </span> */}
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailSent" onClick={(e) => setNavigate('Sent')} className={`navi-link ${navigate === 'Sent' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailSent className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Sent</span>
                                        </a>
                                    </div>
                                    {/* <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailMarked" onClick={(e) => setNavigate('Marked')} className={`navi-link ${navigate === 'Marked' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailMarked className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Marked</span>
                                        </a>
                                    </div> */}
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailSpam" onClick={(e) => setNavigate('Spam')} className={`navi-link ${navigate === 'Spam' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailSpam className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Spam</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabEmailHistory" onClick={(e) => setNavigate('History')} className={`navi-link ${navigate === 'History' ?? 'active'}`}>
                                            <span className="navi-icon mr-4">
                                                <IconMailHistory className="svg-icon svg-icon-lg" />
                                            </span>
                                            <span className="navi-text font-weight-bolder font-size-lg">History</span>
                                        </a>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="offcanvas-mobile-overlay" />
                    <div className="col-md-9 col-sm-12">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabEmailInbox" role="tabpanel" aria-labelledby="tabEmailInbox">
                                {navigate === 'Inbox' && <EmailInbox />}
                            </div>
                            <div className="tab-pane fade" id="tabEmailSent" role="tabpanel" aria-labelledby="tabEmailSent">
                                {navigate === 'Sent' && <EmailSent />}
                            </div>
                            {/* <div className="tab-pane fade" id="tabEmailMarked" role="tabpanel" aria-labelledby="tabEmailMarked">
                                <div>Marked</div>
                            </div> */}
                            <div className="tab-pane fade" id="tabEmailSpam" role="tabpanel" aria-labelledby="tabEmailSpam">
                                <div>spam</div>
                            </div>
                            <div className="tab-pane fade" id="tabEmailHistory" role="tabpanel" aria-labelledby="tabEmailHistory">
                                <div>History</div>
                            </div>
                            <div className="tab-pane fade" id="tabEmailCompose" role="tabpanel" aria-labelledby="tabEmailCompose">
                                {navigate === 'Compose' && <EmailCompose />}
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Email
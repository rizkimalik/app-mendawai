import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { authUser } from 'app/slice/sliceAuth';
import { ButtonRefresh } from 'views/components/button'
import { getEmailInboxData } from 'app/services/apiSosmed';
import { IconMailOpen, IconMailReply, IconTicket } from 'views/components/icon';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { EmailDetail } from '.';

function EmailInbox() {
    // const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const [email_detail, setEmailDetail] = useState('');
    const { email_inbox } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level }))
    }, [dispatch, user]);

    /* function onCreateTicket(data) {
        history.push(`/ticket?thread_id=${data.EMAIL_ID}&channel=Email&account=${data.EFROM}`);
        history.go(0);
    } */
    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <NavLink to={`/ticket?thread_id=${data.EMAIL_ID}&channel=Email&account=${data.EFROM}`} className="btn btn-icon btn-light-primary btn-hover-primary btn-xs mx-1" data-toggle="tooltip" title="Create Ticket">
                    <IconTicket className="svg-icon svg-icon-xs" />
                </NavLink>
                <button type="button" onClick={() => setEmailDetail(data)} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Detail Mail" data-toggle="modal" data-target="#modalEmailDetail">
                    <IconMailOpen className="svg-icon svg-icon-xs" />
                </button>
                <a href={`mailto:${data.EFROM}?subject=RE: ${data.ESUBJECT}`} className="btn btn-icon btn-light-success btn-hover-success btn-xs mx-1" data-toggle="tooltip" title="Reply Mail">
                    <IconMailReply className="svg-icon svg-icon-xs" />
                </a>
            </div>
        )
    }

    return (
        <div className="rounded p-4 my-2">
            {email_detail && <EmailDetail email_detail={email_detail} />}
            <Card>
                <CardHeader className="border-bottom">
                    <CardTitle title="Data Email Inbox" subtitle="" />
                    <CardToolbar>
                        <ButtonRefresh onClick={() => dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level }))} />
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-4">
                    <DataGrid
                        dataSource={email_inbox}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            allowedPageSizes={[10, 20, 50]}
                            displayMode='full'
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" width={130} cellRender={componentButtonActions} />
                        <Column caption="Ticket Number" dataField="TicketNumber" />
                        <Column caption="From" dataField="EFROM" />
                        <Column caption="To" dataField="ETO" />
                        <Column caption="Agent" dataField="agent" />
                        <Column caption="Subject" dataField="ESUBJECT" />
                        <Column caption="Date Email" dataField="Email_Date" />
                        <Column caption="Date Assign" dataField="dateblending" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default EmailInbox
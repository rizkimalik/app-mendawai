import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { ButtonRefresh } from 'views/components/button'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { getEmailSentData } from 'app/services/apiSosmed';
import { IconMailOpen } from 'views/components/icon';
import { EmailDetail } from '.';

function EmailSent() {
    const dispatch = useDispatch();
    const [email_detail, setEmailDetail] = useState('');
    const { email_sent } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(getEmailSentData())
    }, [dispatch]);

    function componentButtonActions({data}) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => setEmailDetail(data)} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Detail Mail" data-toggle="modal" data-target="#modalEmailDetail">
                    <IconMailOpen className="svg-icon svg-icon-xs" />
                </button>
            </div>
        )
    }

    return (
        <div className="rounded rounded p-4 my-2">
            {email_detail && <EmailDetail email_detail={email_detail} />}
            <Card>
                <CardHeader className="border-bottom">
                    <CardTitle title="Data Email Sent" subtitle="" />
                    <CardToolbar>
                        <ButtonRefresh onClick={() => dispatch(getEmailSentData())} />
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-4">
                    <DataGrid
                        dataSource={email_sent}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            allowedPageSizes={[10, 20, 50]}
                            displayMode='full'
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" width={100} cellRender={componentButtonActions} />
                        <Column caption="Email ID" dataField="EMAIL_ID" />
                        <Column caption="From" dataField="EFROM" />
                        <Column caption="To" dataField="ETO" />
                        <Column caption="Ticket Number" dataField="TicketNumber" />
                        <Column caption="Subject" dataField="ESUBJECT" />
                        <Column caption="Date Email" dataField="Email_Date" dataType="datetime" />
                        <Column caption="Agent" dataField="agent" />
                        <Column caption="Email Type" dataField="JENIS_EMAIL" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default EmailSent
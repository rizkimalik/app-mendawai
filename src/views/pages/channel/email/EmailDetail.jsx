import React from 'react'
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';

function EmailDetail({ email_detail }) {
    return (
        <Modal id="modalEmailDetail" modal_size="modal-lg">
            <ModalHeader title={`From: ${email_detail.EFROM}`} />
            <ModalBody className="p-0">
                <div className="row">
                    <div className="col-lg-12 ">
                        <div className="d-flex p-5 flex-row flex-md-row flex-lg-row flex-xxl-row justify-content-between">
                            <div className="d-flex align-items-center">
                                <span className="symbol symbol-50 mr-4 border">
                                    <span className="symbol-label">
                                        <i className="fa fa-envelope text-primary"></i>
                                    </span>
                                </span>
                                <div className="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                    <div className="d-flex">
                                        <span className="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">{email_detail.ESUBJECT}</span>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <div className="toggle-off-item">
                                            <span className="font-weight-bold text-muted cursor-pointer">
                                                From: {email_detail.EFROM}
                                            </span>
                                        </div>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <div className="toggle-off-item">
                                            <span className="font-weight-bold text-muted cursor-pointer">
                                                To: {email_detail.ETO}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-center align-items-xxl-center flex-row flex-md-row flex-lg-row flex-xxl-row">
                                <div className="font-weight-bold text-muted mx-2">{(email_detail.Email_Date).replace('T', ' ').substring(19, 0)}</div>
                                <div className="d-flex align-items-center flex-wrap flex-xxl-nowrap" data-inbox="toolbar">
                                    <span className="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="Settings">
                                        <i className="flaticon-more icon-1x" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="p-5" style={{ height: '400px', overflow: 'auto' }}>
                            <div dangerouslySetInnerHTML={{ __html: email_detail.EBODY_HTML }}></div>
                        </div>
                    </div>
                </div>
            </ModalBody>
            <ModalFooter></ModalFooter>
        </Modal>
    )
}

export default EmailDetail
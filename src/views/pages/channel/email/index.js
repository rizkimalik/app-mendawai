import EmailInbox from './EmailInbox'
import EmailSent from './EmailSent'
import EmailCompose from './EmailCompose'
import EmailDetail from './EmailDetail'

export {
    EmailInbox,
    EmailSent,
    EmailCompose,
    EmailDetail,
}
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { getCallHistoryData, getCallTotal } from 'app/services/apiSosmed'
import { IconCallInbound, IconCallOutbound } from 'views/components/icon'
import { ButtonRefresh } from 'views/components/button'
import CallHistoryInbound from './CallHistoryInbound'
import { IconCall, IconMailHistory, IconGroupChat } from 'views/components/icon'

const Call = () => {
    const dispatch = useDispatch();
    const { call_history, total_call } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(getCallHistoryData());
        dispatch(getCallTotal());
    }, [dispatch]);

    function RefreshData() {
        dispatch(getCallHistoryData());
        dispatch(getCallTotal());
    }


    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Call Center" modul_name="Call Detail Records">
                <NavLink to="/omnichannel/call" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconCall className="svg-icon svg-icon-sm" /> Data Call
                </NavLink>
                <NavLink to="/omnichannel/email" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconMailHistory className="svg-icon svg-icon-sm" /> Data Mailbox
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconGroupChat className="svg-icon svg-icon-sm" /> Data Sosial Media
                </NavLink>
                <ButtonRefresh onClick={() => RefreshData()} />
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-5 col-md-6">
                        <div className="card card-custom gutter-b card-stretch card-shadowless">
                            <div className="card-body p-0">
                                <ul className="dashboard-tabs nav nav-pills nav-danger row row-paddingless m-0 p-0 flex-column flex-sm-row" role="tablist">
                                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                        <a href="#tab_inbound_call" className="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center active" data-toggle="pill">
                                            <div className="d-flex flex-row">
                                                <div className="font-size-h1 font-weight-bolder py-4 px-2 w-auto">{total_call}</div>
                                                <span className="nav-icon py-2 w-auto">
                                                    <IconCallInbound className="svg-icon svg-icon-3x" />
                                                </span>
                                            </div>
                                            <span className="nav-text font-size-lg py-2 font-weight-bold text-center">
                                                Inbound Call
                                            </span>
                                        </a>
                                    </li>
                                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                        <a href="#tab_outbound_call" className="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill">
                                            <div className="d-flex flex-row">
                                                <div className="font-size-h1 font-weight-bolder py-4 px-2 w-auto">0</div>
                                                <span className="nav-icon py-2 w-auto">
                                                    <IconCallOutbound className="svg-icon svg-icon-3x" />
                                                </span>
                                            </div>
                                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                                Outbound Call
                                            </span>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-7 col-md-6">
                        <div className="card card-custom gutter-b border py-6">
                            <div className="card-body d-flex align-items-center justify-content-between flex-wrap">
                                <div className="mr-2">
                                    <h3 className="font-weight-bolder">Call Center</h3>
                                    <div className="text-dark-50 font-size-lg mt-2">
                                        Communication with customers on voice call, video call.
                                    </div>
                                </div>
                                <button className="btn btn-primary font-weight-bold py-3 px-6">Start Now</button>
                            </div>
                        </div>

                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12">
                        <div className="tab-content m-0 p-0">
                            <div className="tab-pane active" id="tab_inbound_call" role="tabpanel">
                                <CallHistoryInbound call_history={call_history} />
                            </div>
                            <div className="tab-pane" id="tab_outbound_call" role="tabpanel">

                            </div>
                        </div>

                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Call

import React from 'react'
import { NavLink } from 'react-router-dom'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { Card, CardBody, CardHeader, CardTitle } from 'views/components/card'
import { getPabxRecording, getPabxToken } from 'app/services/apiSosmed'
import { IconPlay, IconTicket } from 'views/components/icon'
import { SwalAlertError } from 'views/components/SwalAlert'

function CallHistoryInbound({ call_history }) {
    async function playCallDetailRecording(data) {
        const token = await getPabxToken();
        if (token.status === 200) {
            const result = await getPabxRecording({ token: token.data.access_token, interaction_id: data.interaction_id });
            if (result) {
                const window_popup = window.open("", "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=200,width=800,height=500");
                window_popup.document.write(`
                    <video controls width="100%">
                        <source src="${token.data.api_url}/files/${result.file_id}/data" type="video/mp4" />
                    </video>
                `);
            }
            else {
                SwalAlertError('Play Failed', 'File not found.');
            }
        }
        else {
            SwalAlertError(token.message, token.data);
        }
    }

    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                {
                    data.ticket_number === null &&
                    <NavLink to={`/ticket?thread_id=${data.interaction_id}&channel=Call&account=${data.phone_number}`} className="btn btn-icon btn-light-primary btn-hover-primary btn-xs mx-1" data-toggle="tooltip" title="Create Ticket">
                        <IconTicket className="svg-icon svg-icon-sm" />
                    </NavLink>
                }
                <button type="button" onClick={(e) => playCallDetailRecording(data)} className="btn btn-icon btn-light-primary btn-xs py-1 px-2">
                    <IconPlay className="svg-icon svg-icon-sm p-0" />
                </button>
            </div>
        )
    }

    return (
        <div>
            <Card>
                <CardHeader className="border-0">
                    <CardTitle title={`Inbound Call `} subtitle="Data Inbound Calls." />
                </CardHeader>
                <CardBody>
                    <DataGrid
                        dataSource={call_history}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            displayMode='full'
                            allowedPageSizes={[10, 20, 50]}
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" fixed={true} cellRender={componentButtonActions} />
                        <Column caption="Interaction ID" dataField="interaction_id" fixed={true} />
                        <Column caption="Ticket Number" dataField="ticket_number" />
                        <Column caption="Agent" dataField="agent_handle" />
                        <Column caption="Customer ID" dataField="customer_id" />
                        <Column caption="Phone Number" dataField="phone_number" />
                        <Column caption="Callee" dataField="callee" />
                        <Column caption="Caller" dataField="caller" />
                        <Column caption="Start" dataField="start_time" />
                        <Column caption="End" dataField="ended_time" />
                        <Column caption="Ended Reason" dataField="ended_reason" />
                        <Column caption="Ringing" dataField="ring_time" />
                        <Column caption="Answered" dataField="answered_time" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default CallHistoryInbound
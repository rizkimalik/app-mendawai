import React, { useEffect, useRef } from 'react'
import * as echarts from 'echarts';
import HtmlEditor, { Toolbar, Item, MediaResizing } from 'devextreme-react/html-editor';

function General() {
    const chartDom = useRef();

    useEffect(() => {
        const myChart = echarts.init(chartDom.current);
        const option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    dataView: { show: true, readOnly: false },
                    magicType: { show: true, type: ['line', 'bar'] },
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            },
            legend: {
                data: ['Evaporation', 'Precipitation', 'Temperature']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Precipitation',
                    min: 0,
                    max: 250,
                    interval: 50,
                    axisLabel: {
                        formatter: '{value} ml'
                    }
                },
                {
                    type: 'value',
                    name: 'Temperature',
                    min: 0,
                    max: 25,
                    interval: 5,
                    axisLabel: {
                        formatter: '{value} °C'
                    }
                }
            ],
            series: [
                {
                    name: 'Evaporation',
                    type: 'bar',
                    data: [
                        2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3
                    ]
                },
                {
                    name: 'Precipitation',
                    type: 'bar',
                    data: [
                        2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3
                    ]
                },
                {
                    name: 'Temperature',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                }
            ]
        };
        option && myChart.setOption(option);

        window.onSelectPicker();
    }, [])

    return (
        <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div className="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
                <div className="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">

                </div>
            </div>
            <div className="d-flex flex-column-fluid">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-6">
                            <h3>selectpicker</h3>
                            <select className="form-control selectpicker" data-size="7" data-live-search="true">
                                <option data-icon="la la-bullhorn font-size-lg bs-icon" value="AZ">Arizona</option>
                                <option value="CO">Colorado</option>
                                <option value="ID">Idaho</option>
                            </select>
                            <input type="datetime-local" className="form-control form-control-sm" />
                        </div>
                        <div className="col-lg-6">
                            <div style={{ height: '300px', width: '100%' }} ref={chartDom} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12">
                            <HtmlEditor
                                height={300}
                                allowPreview={true}
                            // defaultValue={valueContent}
                            // valueType={editorValueType}
                            // onValueChanged={this.valueChanged}
                            >
                                <MediaResizing enabled={true} />
                                <Toolbar multiline={true}>
                                    <Item name="undo" />
                                    <Item name="redo" />
                                    <Item name="separator" />
                                    <Item
                                        name="size"
                                        acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                                    />
                                    <Item
                                        name="font"
                                        acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                                    />
                                    <Item name="separator" />
                                    <Item name="bold" />
                                    <Item name="italic" />
                                    <Item name="strike" />
                                    <Item name="underline" />
                                    <Item name="separator" />
                                    <Item name="alignLeft" />
                                    <Item name="alignCenter" />
                                    <Item name="alignRight" />
                                    <Item name="alignJustify" />
                                    <Item name="separator" />
                                    <Item name="orderedList" />
                                    <Item name="bulletList" />
                                    <Item name="separator" />
                                    <Item name="color" />
                                    <Item name="background" />
                                    <Item name="separator" />
                                    <Item name="link" />
                                    <Item name="image" />
                                    <Item name="separator" />
                                    <Item name="separator" />
                                    <Item name="clear" />
                                    <Item name="codeBlock" />
                                    <Item name="blockquote" />
                                    <Item name="separator" />
                                    <Item name="insertTable" />
                                    <Item name="deleteTable" />
                                    <Item name="insertRowAbove" />
                                    <Item name="insertRowBelow" />
                                    <Item name="deleteRow" />
                                    <Item name="insertColumnLeft" />
                                    <Item name="insertColumnRight" />
                                    <Item name="deleteColumn" />
                                </Toolbar>
                            </HtmlEditor>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            {/* <DropDownBox
                                // value="1"
                                valueExpr="ID"
                                deferRendering={false}
                                displayExpr="City"
                                placeholder="Select a value..."
                                // showClearButton={true}
                                // dataSource={databox}
                                // onValueChanged={this.syncDataGridSelection}
                                contentRender={() => {
                                    return <TicketReference />
                                }}
                            /> */}


                            {/* <DataGrid
                            dataSource={databox}
                            height={345}
                            hoverStateEnabled={true}
                            selectedRowKeys=""
                            onSelectionChanged=""
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        > */}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default General

import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { apiCustomerRegistered, apiCustomerSyncronize } from 'app/services/apiCustomer';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { IconMark } from 'views/components/icon';

function CustomerSyncronizeModal({ customer }) {
    const dispatch = useDispatch();
    const location = useLocation();
    const { customer_registered } = useSelector(state => state.customer);
    const [fields, setFields] = useState('');
    const history = useHistory()

    useEffect(() => {
        dispatch(apiCustomerRegistered())
    }, [dispatch]);

    function componentButtonActions(data) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={(e) => setFields(data)}
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> {data.customer_id}
                </button>
            </div>
        )
    }

    async function submitSyncronizeCustomer() {
        if (customer.customer_id === fields.customer_id) {
            SwalAlertError('Failed.', 'Cannot sync data same customer.');
        }
        else if (customer.status === 'Registered') {
            SwalAlertError('Failed.', 'Cannot sync data Registered.');
        }
        else {
            const { payload } = await dispatch(apiCustomerSyncronize({
                customer_primary: fields.customer_id,
                customer_sync: customer.customer_id
            }))

            if (payload.status === 200) {
                SwalAlertSuccess('Succsessfuly', payload.data[0].message);
                history.push(`${location.pathname}${location.search}`); //? refresh data
                history.go(0);
            }
            else {
                SwalAlertError('Failed.', payload.data[0].message);
            }
        }
    }

    return (
        <Modal id="modalCustomerSyncronize">
            <ModalHeader title="Customer Syncronize" />
            <ModalBody>
                <div className="list border rounded mb-4">
                    <div className='list-item d-flex align-items-center justify-content-between'>
                        <div className="d-flex align-items-center py-2 mx-2">
                            <span className="bullet bullet-bar bg-warning align-self-stretch mr-2"></span>
                            <div className="symbol symbol-40px">
                                <div className="symbol-label">
                                    <i className="fas fa-user text-warning"></i>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Current</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">{customer.name} - {customer.customer_id}</span>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">To</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">
                                        <span className="svg-icon svg-icon-primary svg-icon-sm">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fillRule="nonzero" />
                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fillRule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Primary</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">{fields.name} - {fields.customer_id}</span>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex flex-column align-items-end mx-2">
                            <button type="button" onClick={submitSyncronizeCustomer} className="btn btn-light-warning font-weight-bold">Syncronize</button>
                        </div>
                    </div>
                </div>

                <DataGrid
                    dataSource={customer_registered}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={5} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[5, 10, 20]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="CustomerID" dataField="customer_id" cellRender={({ data }) => componentButtonActions(data)} />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Email" dataField="email" />
                    <Column caption="Phone Number" dataField="phone_number" />
                    <Column caption="Card ID" dataField="no_ktp" />
                    <Column caption="Address" dataField="address" />
                    <Column caption="Status" dataField="status" cellRender={(data) => {
                        return <span className={`label label-md label-light-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                    }} />
                </DataGrid>
            </ModalBody>
            <ModalFooter />
        </Modal>
    )
}

export default CustomerSyncronizeModal
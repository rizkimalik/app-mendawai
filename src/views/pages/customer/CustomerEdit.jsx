import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiCustomerUpdate, apiCustomerShow } from 'app/services/apiCustomer'
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import CustomerDataChannel from './CustomerDataChannel';
import CustomerSyncronizeModal from './CustomerSyncronizeModal';
import Icons from 'views/components/Icons';
import CustomerDataSync from './CustomerDataSync';

const CustomerEdit = () => {
    const history = useHistory();
    let { customer_id } = useParams();
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const [showModal, setShowModal] = useState('');
    const [customer, setCustomer] = useState('');

    useEffect(() => {
        async function getShowCustomer() {
            try {
                const { payload } = await dispatch(apiCustomerShow({ customer_id }))
                if (payload.status === 200) {
                    const {
                        customer_id,
                        name,
                        email,
                        no_ktp,
                        birth,
                        gender,
                        phone_number,
                        address
                    } = payload.data[0];
                    reset({
                        customer_id,
                        name,
                        email,
                        no_ktp,
                        birth: birth?.slice(0, 10),
                        gender,
                        phone_number,
                        address
                    });
                    setCustomer(payload.data[0]);
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getShowCustomer();
    }, [customer_id, reset, dispatch]);

    const onSubmitUpdateCustomer = async (data) => {
        try {
            const { payload } = await dispatch(apiCustomerUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data customer!')
                history.push('/customer')
            }
            else if (payload.status === 201) {
                SwalAlertError('Already Exists.', payload.data);
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Failed Insert.', 'Please try again.');
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Customer Profile" menu_name="Customer" modul_name="Customer Edit" />
            <Container>
                <div className="row">
                    <div className="col-lg-4">
                        <Card>
                            <CardHeader>
                                <CardTitle title="Profile Customer" subtitle="Form Edit customer." />
                                <CardToolbar>
                                    <button onClick={(e) => setShowModal('CustomerSync')} className="btn btn-sm btn-light-warning" data-toggle="modal" data-target="#modalCustomerSyncronize">
                                        <Icons iconName="substract" className="svg-icon svg-icon-md" /> Syncronize To
                                    </button>
                                </CardToolbar>
                            </CardHeader>
                            <form onSubmit={handleSubmit(onSubmitUpdateCustomer)} className="form">
                                <CardBody>
                                    <FormInput
                                        name="name"
                                        type="text"
                                        label="Full Name"
                                        className="form-control"
                                        placeholder="Enter Full Name"
                                        register={register}
                                        rules={{ required: true, maxLength: 100 }}
                                        readOnly={false}
                                        errors={errors.name}
                                    />
                                    <FormInput
                                        name="email"
                                        type="email"
                                        label="Email"
                                        className="form-control"
                                        placeholder="Enter Email"
                                        register={register}
                                        rules={{ required: true, pattern: /^\S+@\S+$/i }}
                                        readOnly={false}
                                        errors={errors.email}
                                    />
                                    <FormInput
                                        name="phone_number"
                                        type="number"
                                        label="Phone Number"
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                        register={register}
                                        rules={{ pattern: /^[0-9]+$/i }}
                                        readOnly={false}
                                        errors={errors.phone_number}
                                    />
                                    <FormInput
                                        name="no_ktp"
                                        type="number"
                                        label="Card ID"
                                        className="form-control"
                                        placeholder="Enter CardID"
                                        register={register}
                                        rules={{ pattern: /^[0-9]+$/i }}
                                        readOnly={false}
                                        errors={errors.no_ktp}
                                    />
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <label>Gender:</label>
                                            <select className="form-control" {...register("gender", { required: true })}>
                                                <option value="">-- select gender--</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                            {errors.gender && <span className="form-text text-danger">Select enter gender</span>}
                                        </div>
                                    </div>
                                    <FormInput
                                        name="birth"
                                        type="date"
                                        label="Birth"
                                        className="form-control"
                                        placeholder="Enter Birth"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.birth}
                                    />
                                    <FormInput
                                        name="address"
                                        type="textarea"
                                        label="Address"
                                        className="form-control"
                                        placeholder="Enter Address"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.address}
                                    />
                                </CardBody>
                                <CardFooter>
                                    <ButtonCancel to="/customer" />
                                    <ButtonSubmit />
                                </CardFooter>
                            </form>
                        </Card>
                    </div>
                    <div className="col-lg-8">
                        <CustomerDataChannel customer_id={customer_id} />

                        <h5 className="font-weight-bolder mt-5">Data Syncronized</h5>
                        <CustomerDataSync customer_id={customer_id} />
                    </div>
                </div>
                {showModal === 'CustomerSync' && <CustomerSyncronizeModal customer={customer} />}
            </Container>
        </MainContent>
    )
}

export default CustomerEdit

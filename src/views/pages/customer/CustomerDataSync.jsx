import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiCustomer_DataSync } from 'app/services/apiCustomer';

function CustomerDataSync({ customer_id }) {
    const dispatch = useDispatch();
    const { customer_syncronize } = useSelector(state => state.customer);

    useEffect(() => {
        if (customer_id) {
            dispatch(apiCustomer_DataSync({ syncronized_to: customer_id }))
        }
        else {
            dispatch(apiCustomer_DataSync())
        }
    }, [dispatch, customer_id]);

    return (
        <div>
            <DataGrid
                dataSource={customer_syncronize}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    allowedPageSizes={[10, 20, 50]}
                    displayMode='full'
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Status" dataField="status" cellRender={(data) => {
                    return <span className={`label label-md label-light-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                }} />
                <Column caption="CustomerID" dataField="customer_id" />
                <Column caption="Sync To" dataField="syncronized_to" />
                <Column caption="Name" dataField="name" />
                <Column caption="Email" dataField="email" />
                <Column caption="Phone Number" dataField="phone_number" />
                <Column caption="Card ID" dataField="no_ktp" />
                <Column caption="Source" dataField="source" />
                <Column caption="Gender" dataField="gender" />
                <Column caption="Address" dataField="address" />
            </DataGrid>
        </div>
    )
}

export default CustomerDataSync
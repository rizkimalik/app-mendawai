import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { apiCustomerStore } from 'app/services/apiCustomer'
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';

const CustomerCreate = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit } = useForm();

    const onSubmitCreateCustomer = async (data) => {
        try {
            const { payload } = await dispatch(apiCustomerStore(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Insert Success', 'Success into application!')
                history.push('/customer')
            }
            else if (payload.status === 201) {
                SwalAlertError('Already Exists.', payload.data);
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Failed Insert.', 'Please try again.');
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Customer Create" menu_name="Customer" modul_name="Customer Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form New Customer" subtitle="Form add new customer." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateCustomer)} className="form">
                        <CardBody>
                            <div className="row">
                                <div className="col-lg-6">
                                    <FormInput
                                        name="name"
                                        type="text"
                                        label="Full Name"
                                        className="form-control"
                                        placeholder="Enter Full Name"
                                        register={register}
                                        rules={{ required: true, maxLength: 100 }}
                                        readOnly={false}
                                        errors={errors.name}
                                    />
                                </div>
                                <div className="col-lg-6">
                                    <FormInput
                                        name="no_ktp"
                                        type="number"
                                        label="Card ID"
                                        className="form-control"
                                        placeholder="Enter CardID"
                                        register={register}
                                        rules={{ pattern: /^[0-9]+$/i }}
                                        readOnly={false}
                                        errors={errors.no_ktp}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                    <FormInput
                                        name="email"
                                        type="email"
                                        label="Email"
                                        className="form-control"
                                        placeholder="Enter Email"
                                        register={register}
                                        rules={{ required: true, pattern: /^\S+@\S+$/i }}
                                        readOnly={false}
                                        errors={errors.email}
                                    />
                                </div>
                                <div className="col-lg-6">
                                    <FormInput
                                        name="phone_number"
                                        type="number"
                                        label="Phone Number"
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                        register={register}
                                        rules={{ pattern: /^[0-9]+$/i }}
                                        readOnly={false}
                                        errors={errors.phone_number}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                    <label>Gender:</label>
                                    <select className="form-control" {...register("gender", { required: true })}>
                                        <option value="">-- select gender--</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    {errors.gender && <span className="form-text text-danger">Select enter gender</span>}
                                </div>
                                <div className="col-lg-6">
                                    <FormInput
                                        name="birth"
                                        type="date"
                                        label="Birth"
                                        className="form-control"
                                        placeholder="Enter Birth"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.birth}
                                    />
                                </div>
                            </div>
                            <FormInput
                                name="address"
                                type="textarea"
                                label="Address"
                                className="form-control"
                                placeholder="Enter Address"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.address}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/customer" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default CustomerCreate

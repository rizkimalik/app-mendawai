import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import Icons from 'views/components/Icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import { apiCustomerJourney } from 'app/services/apiCustomer';
import { ButtonRefresh } from 'views/components/button';
import { apiTicketShow } from 'app/services/apiTicket';
import { TicketUpdate } from '../ticket';

const CustomerJourney = ({ customer }) => {
    const dispatch = useDispatch();
    const { journey } = useSelector(state => state.customer);

    useEffect(() => {
        dispatch(apiCustomerJourney({ customer_id: customer.customer_id }))
    }, [dispatch, customer])

    return (
        <div>
            <Modal id="modalJourneyCustomer" modal_size="modal-lg">
                <ModalHeader title={`Data Customer Journey #${customer.customer_id}`} />
                <ModalBody className="p-0">
                    <div style={{ height: '450px', overflow: 'auto' }}>
                        <div className="timeline timeline-3 px-20 py-5">
                            <div className="timeline-items">
                                {
                                    journey.map((item, index) => {
                                        let status = '';
                                        if (item.status === 'Open') {
                                            status = 'label-light-primary';
                                        }
                                        else if (item.status === 'Pending') {
                                            status = 'label-light-warning';
                                        }
                                        else if (item.status === 'Progress') {
                                            status = 'label-light-info';
                                        }
                                        else if (item.status === 'Closed') {
                                            status = 'label-light-success';
                                        }

                                        let icon = '';
                                        if (item.ticket_source === 'Call') {
                                            icon = 'fa fa-phone text-primary';
                                        }
                                        else if (item.ticket_source === 'Email') {
                                            icon = 'fa fa-mail-bulk text-danger';
                                        }
                                        else if (item.ticket_source === 'Chat') {
                                            icon = 'fa fa-comments text-warning';
                                        }
                                        else if (item.ticket_source === 'Facebook') {
                                            icon = 'fab fa-facebook text-primary';
                                        }
                                        else if (item.ticket_source === 'Twitter') {
                                            icon = 'fab fa-twitter text-primary';
                                        }
                                        else if (item.ticket_source === 'Instagram') {
                                            icon = 'fab fa-instagram text-info';
                                        }
                                        else if (item.ticket_source === 'Whatsapp') {
                                            icon = 'fab fa-whatsapp text-success';
                                        }

                                        return <div className="timeline-item" key={index}>
                                            <div className="timeline-media">
                                                <i className={`${icon}`} />
                                            </div>
                                            <div className="timeline-content bg-white border">
                                                <div className="d-flex align-items-center justify-content-between mb-2">
                                                    <div className="mr-2">
                                                        <button type="button" onClick={(e) => dispatch(apiTicketShow({ ticket_number: item.ticket_number }))} className="btn btn-sm btn-light-primary py-1 px-2 font-weight-bold" data-toggle="modal" data-target="#modalUpdateTicket">
                                                            <Icons iconName="ticket" className="svg-icon svg-icon-sm" />
                                                            {item.ticket_number}
                                                        </button>
                                                    </div>
                                                    <div>
                                                        <span className={`label ${status} font-weight-bolder label-inline`}>{item.status}</span>
                                                    </div>
                                                </div>
                                                <div className="d-flex align-items-center justify-content-between">

                                                    <div className="d-flex flex-column font-size-xs">
                                                        <span>InteractionID : {item.thread_id}</span>
                                                        <span>Account : {item.account_id}</span>
                                                    </div>
                                                    <div>
                                                        <span className="d-flex flex-column font-size-xs">
                                                            <span>Channel: {item.ticket_source} - {item.source_information}</span>
                                                            <span>{item.time} {item.date}</span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <span className="font-size-xs">Subject : {item.subject}</span>
                                                </div>
                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <span className="label font-weight-bolder label-inline">Total Journey: {journey.length}</span>
                    <ButtonRefresh onClick={() => dispatch(apiCustomerJourney({ customer_id: customer.customer_id }))} />
                </ModalFooter>
            </Modal>
            <TicketUpdate />
        </div>
    )
}

export default CustomerJourney
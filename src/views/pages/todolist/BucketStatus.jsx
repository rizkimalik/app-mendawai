import React from 'react'
import { NavLink } from 'react-router-dom';
// import Icons from 'views/components/Icons';
const bg_card = ['bg-primary', 'bg-warning', 'bg-success', 'bg-danger', 'bg-info', 'bg-light', 'bg-dark'];

function BucketStatus({ total_ticket }) {
    return (
        <div className="row">
            {
                total_ticket.map((item, index) => {
                    // let background, icon;
                    // if (item.total === 0) {
                    //     background = 'bg-warning'
                    //     icon = 'flag'
                    // } else {
                    //     background = 'bg-primary'
                    //     icon = 'open'
                    // }

                    return (
                        <div className="col-md-3" key={index}>
                            <NavLink to={`/todolist/${item.status}`} className={`card card-custom ${bg_card[index]} card-stretch gutter-b`}>
                                <div className="card-body">
                                    {/* <Icons iconName={`${icon}`} className="svg-icon svg-icon-3x svg-icon-white" /> */}
                                    <i className={`icon-2x text-white ${item.icon}`} />
                                    <span className="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{item.total}</span>
                                    <span className="font-weight-bold text-white font-size-sm">Ticket {item.status}</span>
                                </div>
                            </NavLink>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default BucketStatus
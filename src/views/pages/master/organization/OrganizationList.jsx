import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import DataGrid, { Column, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiOrganizationDelete, apiOrganizationList } from 'app/services/apiOrganization';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';

function OrganizationList() {
    const dispatch = useDispatch();
    const { organizations } = useSelector(state => state.organization)

    useEffect(() => {
        dispatch(apiOrganizationList())
    }, [dispatch]);

    async function deleteOrganizationHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiOrganizationDelete({ id }));
                if (payload.status === 200) {
                    SwalAlertSuccess('Success Delete', 'Deleted from database.')
                    dispatch(apiOrganizationList())
                }
                else {
                    SwalAlertError('Failed Delete', 'Please try again.')
                }
            }
        });
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Organization" modul_name="">
                <ButtonCreate to="/organization/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Organization" subtitle="data organization." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => dispatch(apiOrganizationList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={organizations}
                            keyExpr="id"
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Actions" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex align-items-end justify-content-center">
                                        <ButtonEdit to={`organization/${data.id}/edit`} />
                                        <ButtonDelete onClick={(e) => deleteOrganizationHandler(data.id)} />
                                    </div>
                                )
                            }} />
                            <Column caption="Organization Name" dataField="organization_name" />
                            <Column caption="Description" dataField="description" />
                            <Column caption="Created By" dataField="created_by" />
                            <Column caption="Created At" dataField="created_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Updated By" dataField="updated_by" />
                            <Column caption="Updated At" dataField="updated_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Active" dataField="active" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex justify-content-center">
                                        <span className="switch switch-outline switch-icon switch-sm switch-primary">
                                            <label>
                                                <input type="checkbox" defaultChecked={data.active} />
                                                <span />
                                            </label>
                                        </span>
                                    </div>
                                )
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default OrganizationList
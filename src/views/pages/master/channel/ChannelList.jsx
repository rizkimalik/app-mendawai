import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import DataGrid, { Column, FilterRow, Grouping, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiChannelDelete, apiChannelList } from 'app/services/apiChannel';
import { SwalAlertSuccess } from 'views/components/SwalAlert';

function ChannelList() {
    const dispatch = useDispatch();
    const { channels } = useSelector(state => state.channel)

    useEffect(() => {
        dispatch(apiChannelList())
    }, [dispatch]);

    async function deleteChannelHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiChannelDelete({ id }));
                if (payload.status === 200) {
                    SwalAlertSuccess('Success Delete', `deleted from database!`);
                    dispatch(apiChannelList())
                }
            }
        });
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Channel Ticket" modul_name="">
                <ButtonCreate to="/channel/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Channel Ticket" subtitle="data channel ticket." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => dispatch(apiChannelList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={channels}
                            keyExpr="id"
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <GroupPanel visible={true} />
                            <Grouping autoExpandAll={true} />
                            <Column caption="Actions" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex align-items-end justify-content-center">
                                        <ButtonEdit to={`channel/${data.id}/edit`} />
                                        <ButtonDelete onClick={(e) => deleteChannelHandler(data.id)} />
                                    </div>
                                )
                            }} />
                            <Column caption="Channel Ticket" dataField="channel" groupIndex={0} />
                            <Column caption="Channel Type" dataField="channel_type" />
                            <Column caption="Description" dataField="description" />
                            <Column caption="Created By" dataField="created_by" />
                            <Column caption="Created At" dataField="created_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Updated By" dataField="updated_by" />
                            <Column caption="Updated At" dataField="updated_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Active" dataField="active" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex justify-content-center">
                                        <span className="switch switch-outline switch-icon switch-sm switch-primary">
                                            <label>
                                                <input type="checkbox" defaultChecked={data.active} />
                                                <span />
                                            </label>
                                        </span>
                                    </div>
                                )
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChannelList
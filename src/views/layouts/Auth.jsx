import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { axiosDefault } from 'app/config';
import { Header, Aside, Footer, PanelAux } from './partials';
import SocketIO from 'views/components/SocketIO';
import { AskPermission } from 'views/components/Notification';
import { authUser } from 'app/slice/sliceAuth';
import { getMainMenu } from 'app/services/apiMenu'
import Icons from 'views/components/Icons';
import { IconCall } from 'views/components/icon';

function Auth({ children }) {
    const auth = useSelector(authUser);
    const dispatch = useDispatch();
    const [showAux, setShowAux] = useState(''); //offcanvas-on = showpanel
    const { main_menu } = useSelector(state => state.mainmenu);

    useEffect(() => {
        AskPermission();
        axiosDefault(auth.token);
        dispatch(getMainMenu({ user_level: auth.user_level }))
    }, [dispatch, auth]);

    return (
        <div className="d-flex flex-row flex-column-fluid page">
            {auth.user_level === 'L1' && <SocketIO />}
            <Aside main_menu={main_menu} />

            <div className="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <Header />
                {children}
                <Footer />
            </div>

            {/* tooltip icon */}
            <ul className="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
                <li className="nav-item mb-2" data-toggle="tooltip" data-placement="right" data-original-title="AUX">
                    <button onClick={() => setShowAux('offcanvas-on')} className="btn btn-sm btn-icon btn-bg-light btn-icon-success btn-hover-success">
                        <Icons iconName="setting" className="svg-icon svg-icon-md" />
                    </button>
                </li>
                <li className="nav-item mb-2" data-toggle="tooltip" data-placement="left" data-original-title="Call Center">
                    <NavLink to="/omnichannel/call" className="btn btn-sm btn-icon btn-bg-light btn-icon-danger btn-hover-danger">
                        <IconCall className="svg-icon svg-icon-md" />
                    </NavLink>
                </li>
                <li className="nav-item" data-toggle="tooltip" data-placement="left" data-original-title="Todolist">
                    <NavLink to="/todolist" className="btn btn-sm btn-icon btn-bg-light btn-icon-warning btn-hover-warning">
                        <i className="fa fa-clipboard-list" />
                    </NavLink>
                </li>
            </ul>

            {showAux !== '' && <PanelAux showAux={showAux} setShowAux={setShowAux} />}
        </div>
    )
}

export default Auth

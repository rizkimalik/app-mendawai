import React, { useEffect } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import { axiosDefault, socket } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import Icons from 'views/components/Icons';
import { apiCheckAuthExpired, useLogoutMutation } from 'app/services/apiAuth';
import { apiStatusAuxUser } from 'app/services/apiAux';
import { apiTodolistNotification } from 'app/services/apiTodolist';
import { getSosmedNotification } from 'app/services/apiSosmed';

function Header() {
    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const [logout] = useLogoutMutation();
    const { aux_status } = useSelector(state => state.aux);
    const { data_notification } = useSelector(state => state.todolist);
    const { sosmed_notifications } = useSelector(state => state.sosialmedia);
    const pathname = history.location.pathname;

    useEffect(() => {
        axiosDefault(user.token);
        async function onCheckAuth() {
            const { payload } = await dispatch(apiCheckAuthExpired({ token: user.token }));
            if (!payload) {
                console.log('auth token expired.');
                Swal.fire({
                    title: 'Auth token expired.',
                    text: 'Please re-login the application.',
                    buttonsStyling: false,
                    icon: "warning",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                }).then(() => {
                    localStorage.clear();
                    window.location.reload();
                });
            }
        }
        onCheckAuth();
    }, [dispatch, user, pathname]);
    
    useEffect(() => {
        axiosDefault(user.token);
        dispatch(apiStatusAuxUser({ username: user.username }));
        // dispatch(getSosmedNotification({ agent_handle: user.username }));
        // dispatch(apiTodolistNotification({
        //     user_create: user.username,
        //     department_id: user.department,
        //     user_level: user.user_level,
        //     status: 'Open'
        // }));
    }, [dispatch, user]);

    async function onSignOut() {
        try {
            const response = await logout({ username: user.username });
            if (response.data.status === 200) {
                socket.disconnect();
                localStorage.clear();
                history.push('/login');
                window.location.reload();
            }
        }
        catch (error) {
            console.log(error)
        }
    }

    return (
        <header id="kt_header" className="header header-fixed">
            <div className="container-fluid d-flex align-items-stretch justify-content-between">
                <div className="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu" className="header-menu header-menu-mobile header-menu-layout-default">
                        <ul className="menu-nav">
                            <li className="menu-item menu-item-submenu" data-menu-toggle="click">
                                <NavLink to="/omnichannel/socmed" className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                                    {/* <Icons iconName="group-chat" className="svg-icon svg-icon-xl svg-icon-info" /> */}
                                    <i className="flaticon-chat text-info"></i>
                                </NavLink>
                            </li>
                            <li className="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click">
                                <NavLink to="/omnichannel/email" className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                                    {/* <Icons iconName="notification" className="svg-icon svg-icon-xl svg-icon-success" /> */}
                                    <i className="flaticon-multimedia text-success"></i>

                                </NavLink>
                            </li>
                            {/* <li className="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click">
                                <NavLink to="/omnichannel/call" className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                                    <Icons iconName="notification" className="svg-icon svg-icon-xl svg-icon-danger" />
                                </NavLink>
                            </li> */}
                        </ul>
                    </div>
                </div>

                <div className="topbar">
                    <div className="dropdown">
                        <div className="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                            <div onClick={() => dispatch(apiTodolistNotification({ user_create: user.username, department_id: user.department, user_level: user.user_level, status: 'Open' }))} className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-info">
                                {/* <Icons iconName="incoming-box" className="svg-icon svg-icon-xl svg-icon-info" /> */}
                                <i className="flaticon-interface-3 text-info"></i>
                                {data_notification?.length > 0 && <span className="pulse-ring" />}
                            </div>
                        </div>
                        <div className="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                            <div className="d-flex flex-column flex-center py-5 bg-light-info bgi-size-cover bgi-no-repeat rounded-top">
                                <span className="btn btn-info btn-sm font-weight-bold font-size-lg mt-2">{data_notification?.length} Ticket Open</span>
                            </div>
                            <div className="row row-paddingless">
                                <div className="col-12" style={{ height: 300, overflow: 'auto' }}>
                                    <div className="navi navi-hover scroll my-4 ps" data-scroll="true">
                                        {
                                            data_notification?.map((data, index) => {
                                                return (
                                                    <NavLink to="/todolist/Open" className="navi-item" key={index}>
                                                        <div className="navi-link">
                                                            <div className="navi-icon mr-2">
                                                                <Icons iconName="ticket" className="svg-icon svg-icon-xl svg-icon-info" />
                                                            </div>
                                                            <div className="navi-text">
                                                                <div className="font-weight-bold"><b>#{data.ticket_number} - {data.status}</b></div>
                                                                <p>Via channel {data.ticket_source}, created by <b>{data.user_create}</b></p>
                                                                <div className="text-muted">{data.date_create}</div>
                                                            </div>
                                                        </div>
                                                    </NavLink>
                                                )
                                            })
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    {/* <div className="dropdown">
                        <div className="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                            <div onClick={() => dispatch(getSosmedNotification({ agent_handle: user.username }))} className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                                <Icons iconName="chat" className="svg-icon svg-icon-xl svg-icon-primary" />
                                {sosmed_notifications?.length > 0 && <span className="pulse-ring" />}
                            </div>
                        </div>
                        <div className="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                            <div className="d-flex flex-column flex-center py-5 bg-light-primary bgi-size-cover bgi-no-repeat rounded-top">
                                <span className="btn btn-primary btn-sm font-weight-bold font-size-lg mt-2">{sosmed_notifications?.length} Messages</span>
                            </div>
                            <div className="row row-paddingless">
                                <div className="col-12" style={{ height: 300, overflow: 'auto' }}>
                                    <div className="navi navi-hover scroll my-4 ps" data-scroll="true">
                                        {
                                            sosmed_notifications?.map((data, index) => {
                                                return (
                                                    <NavLink to="/omnichannel/socmed" className="navi-item" key={index}>
                                                        <div className="navi-link">
                                                            <div className="navi-icon mr-2">
                                                                <Icons iconName="chat" className="svg-icon svg-icon-xl svg-icon-primary" />
                                                            </div>
                                                            <div className="navi-text">
                                                                <div className="font-weight-bold">{data.name} - {data.user_id}</div>
                                                                <p>Via {data.channel}, {data.message}</p>
                                                                <div className="text-muted">{data.date_create}</div>
                                                            </div>
                                                        </div>
                                                    </NavLink>
                                                )
                                            })
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div> */}

                    <div className="dropdown">
                        <div className="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                            <div className="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2">
                                <span className="label label-md label-light-success label-inline mr-1">{aux_status.aux_status}</span>
                                <span className="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{user.name}</span>
                                <span className="symbol symbol-lg-35 symbol-25 symbol-light-info">
                                    <span className="symbol-label font-size-h5 font-weight-bold text-uppercase">{user.name.slice(0, 1)}</span>
                                </span>
                            </div>
                        </div>

                        <div className="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
                            <ul className="navi navi-hover py-4">
                                <li className="navi-item active">
                                    <Link to={`/user/${user.id}/edit`} className="navi-item">
                                        <div className="navi-link">
                                            <div className="symbol symbol-20 mr-3">
                                                <div className="symbol-label">
                                                    <Icons iconName="profile" className="svg-icon svg-icon-lg svg-icon-info" />
                                                </div>
                                            </div>
                                            <div className="navi-text">
                                                <div className="font-weight-bold">Profile</div>
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <li className="navi-item active">
                                    <Link to="/todolist" className="navi-item">
                                        <div className="navi-link">
                                            <div className="symbol symbol-20 mr-3">
                                                <div className="symbol-label">
                                                    <Icons iconName="open" className="svg-icon svg-icon-lg svg-icon-info" />
                                                </div>
                                            </div>
                                            <div className="navi-text">
                                                <div className="font-weight-bold">Todolist</div>
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <li className="navi-item active">
                                    <Link to="/login" className="navi-item" onClick={onSignOut}>
                                        <div className="navi-link">
                                            <div className="symbol symbol-20 mr-3">
                                                <div className="symbol-label">
                                                    <Icons iconName="sign-out" className="svg-icon svg-icon-lg svg-icon-info" />
                                                </div>
                                            </div>
                                            <div className="navi-text">
                                                <div className="font-weight-bold">Sign Out</div>
                                            </div>
                                        </div>
                                    </Link>
                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header

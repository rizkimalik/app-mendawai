import React, { memo } from 'react'
import { NavLink } from 'react-router-dom'
import ModulItem from './ModulItem';
// import Icons from 'views/components/Icons';

function MenuItem({ item, active }) {
    return (
        <>
            {
                Boolean(item.is_root) === false
                    ? <li
                        className={
                            active === item.path
                                ? "menu-item menu-item-active"
                                : "menu-item"
                        }
                    >
                        <NavLink exact to={item.path} className="menu-link inline-flex">
                            {/* <Icons iconName={item.icon} className="svg-icon menu-icon" /> */}
                            <i className={`menu-icon ${item.icon}`}></i>
                            <span className="menu-text ml-2">{item.menu_name}</span>
                        </NavLink>
                    </li>

                    : <li className="menu-item menu-item-submenu" data-menu-toggle="hover">
                        <button className="btn btn-link menu-link inline-flex menu-toggle">
                            {/* <Icons iconName={item.icon} className="svg-icon menu-icon" /> */}
                            <i className={`menu-icon ${item.icon}`}></i>

                            <span className="menu-text">{item.menu_name}</span>
                            <i className="menu-arrow" />
                        </button>

                        {
                            item.menu_modul.length > 0 && 
                            <ModulItem data={item.menu_modul} active={active} key={item.menu_id} />
                        }
                    </li>
            }
        </>
    )
}

export default memo(MenuItem)

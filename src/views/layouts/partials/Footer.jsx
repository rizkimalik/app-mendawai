import React from 'react'

function Footer() {
    return (
        <footer className="footer bg-white py-4 d-flex flex-lg-column shadow-none border-top" id="kt_footer">
            <div className="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
                <div className="text-dark order-2 order-md-1 pt-2">
                    <span className="text-muted font-weight-bold mr-2">2022 ©</span>
                    <span className="text-dark-75 text-hover-primary">Mendawai Helpdesk</span>
                </div>
            </div>
        </footer>
    )
}

export default Footer

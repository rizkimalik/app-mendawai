import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Auth from 'views/layouts/Auth'

const PrivateRoute = ({ children, path, isAuth, ...rest }) => (
    <Route
        {...rest}
        path={path}
        render={({ location }) =>
            isAuth ? (
                <Auth>{children}</Auth>
            ) : (
                <Redirect to={{ pathname: '/login', state: { from: location } }} />
            )
        }
    />
)

export default React.memo(PrivateRoute);

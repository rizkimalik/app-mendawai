import routes from './routes';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute'; //Auth Route
import ProtectedRoutes from './ProtectedRoutes';

export {
    routes,
    PublicRoute,
    PrivateRoute,
    ProtectedRoutes,
}

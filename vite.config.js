import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src/"),
            "app": path.resolve(__dirname, "./src/app"),
            "assets": path.resolve(__dirname, "./src/assets"),
            "router": path.resolve(__dirname, "./src/router"),
            "views": path.resolve(__dirname, "./src/views"),
            "devextreme_grid": 'devextreme-react/data-grid',
            "devextreme_store": 'devextreme/data/custom_store'
        },
    },
    server: {
        port: 3000,
    },
    build: {
        outDir: path.join(__dirname, "build"),
        rollupOptions: {
            treeshake: false,
            output: {
                manualChunks(id) {
                    // Memisahkan file yang berasal dari node_modules ke dalam chunk tersendiri
                    if (id.includes('node_modules')) {
                        return 'vendor';
                    }
                    // Memisahkan file yang berasal dari folder components ke dalam chunk tersendiri
                    if (id.includes('src/app')) {
                        return 'app';
                    }
                    if (id.includes('src/router')) {
                        return 'router';
                    }
                    if (id.includes('src/views')) {
                        return 'views';
                    }
                },
            },
        },
        sourcemap: false
    },
});